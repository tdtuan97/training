<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'product'], function(){
        Route::get('/','MstProductController@index');
        Route::get('/index','MstProductController@index');
        Route::post('/load','MstProductController@load')->name('load');
        Route::post('/search','MstProductController@search')->name('search');
        Route::post('/insert','MstProductController@insert')->name('insertProduct');
        Route::post('/update','MstProductController@update')->name('updateProduct');
        Route::post('/delete','MstProductController@delete')->name('deleteProduct');
        Route::post('/getProductByCode','MstProductController@getProductByCode')->name('getProductByCode');
        Route::post('/changeStatusProduct','MstProductController@changeStatusProduct')->name('changeStatusProduct');
        Route::post('/importFromCSV','MstProductController@importFromCSV')->name('importFromCSV');
        Route::post('/exportToCSV','MstProductController@exportToCSV')->name('exportToCSV');
    });
    Route::group(['prefix' => 'user'], function(){
        Route::get('/','MstUserController@index')->name('index');
        Route::get('/index','MstUserController@index');
        Route::post('/load','MstUserController@load');
        Route::post('/active','MstUserController@changeStatusUser');
        Route::post('/update','MstUserController@update');
        Route::post('/change-group','MstUserController@changeGroupUser');
        Route::post('/get-group','MstUserController@getGroupUserLogin');
        Route::post('/get-user-by-email','MstUserController@getUserByEmail')->name('get-user-by-email');
        Route::post('/get-user-by-id','MstUserController@getUserById')->name('get-user-by-id');
    });
    Route::group(['prefix' => 'group-user'], function(){
        Route::get('/','GroupUserController@index');
        Route::get('/index','GroupUserController@index');
        Route::post('/load','GroupUserController@load');
        Route::post('/update','GroupUserController@update');
        Route::post('/change-rule','GroupUserController@changeRule');
        Route::post('/get-all','GroupUserController@getAllGroupUser')->name('get-all-group-user');
        Route::post('/get-group-by-name','GroupUserController@getGroupByName')->name('get-group-by-name');
        Route::post('/get-group-by-id','GroupUserController@getGroupById')->name('get-group-by-id');
        Route::post('/get-rules-by-group-id','GroupUserController@getRulesByGroupId')->name('get-rules-by-group-id');
    });
    Route::group(['prefix' => 'rule-user'], function(){
        Route::post('/get-all','RuleController@getAllRule')->name('get-all-rule-user');
    });
    Route::group(['prefix' => 'order'], function(){
        Route::any('/','OrderController@index');
        Route::any('/index','OrderController@index');
    });
});
