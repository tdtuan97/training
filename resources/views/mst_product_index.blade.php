@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div>
        <button id="btnAddProduct" class="btn btn-primary">Add</button>
    </div>
    <div>
        <h3>廃盤 欠品 販売</h3>
    </div>
    <form action="{{route('exportToCSV')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>

        <div id="formInput">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="checkbox" name="checkboxCheetahStatus2" id="checkboxCheetahStatus2">廃番
            <input type="checkbox" name="checkboxCheetahStatus1" id="checkboxCheetahStatus1">欠品
            <input type="checkbox" name="checkboxCheetahStatus0" id="checkboxCheetahStatus0">販売
            <label>JANコード</label>
            <input type="text" name="txtProductJan" id="txtProductJan" value=""/>
            <label>メーカー名</label>
            <input type="text" name="txtMakerFullNm" id="txtMakerFullNm" value=""/>
            <label>品番</label>
            <input type="text" name="txtProductCode" id="txtProductCode" value=""/>
            <label>商品名</label>
            <input type="text" name="txtProductName" id="txtProductName" value=""/>
            <input class="btn btn-primary" name="btnSearch" id="btnSearch" type="button" value="検索する"/>
            <input class="btn btn-primary" name="btnCancel" id="btnCancel" type="button" value="元に戻す"/>
        </div>
        <div>
            <div class="col-lg-4">
                <input type="submit" class="btn btn-info" value="Export"/>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalImportCSV">Import
                </button>
            </div>
            <div class="groupButton col-lg-8">
                <a href="#" id="btnModalChangeStatus1" class="btn btn-danger" data-toggle="modal"
                   data-target="#modalChangeStatus1">廃番にする</a>
                <a href="#" id="btnModalChangeStatus2" class="btn btn-warning" data-toggle="modal"
                   data-target="#modalChangeStatus2">欠品にする</a>
                <a href="#" id="btnModalChangeStatus3" class="btn btn-success" data-toggle="modal"
                   data-target="#modalChangeStatus3">販売にする</a>
            </div>
        </div>
    </form>
    <br/>
    <div class="labelCount">
        <select id="selectPageSize">
            <option value="50" selected="selected">50</option>
            <option value="100">100</option>
            <option value="250">250</option>
            <option value="500">500</option>
            <option value="1000">1000</option>
        </select>
        <label>Từ </label>
        <label id="lbFrom"></label>
        <label> Đến </label>
        <label id="lbTo"></label>
        <label> Trong </label>
        <label id="lbTotal"></label>
        <label> Kết quả</label>
    </div>
    <div class="checkAllCheckBox">
        <button id="btnCheck" onclick="checkAll()" class="btn btn-success">+全て選択</button>
        <button id="btnUnCheck" onclick="UnCheckAll()" class="btn btn-danger">-選択解除</button>
    </div>
    <div class="myPagination">
    </div>
    <div class="imgLoading">
        <img src="{{ asset('/img/loading.gif') }}" alt="">
    </div>
    <table id="tableProduct">
        <thead>
        <tr>
            <th class="abc col-lg-1">
                ステータス
            </th>
            <th class="col-lg-2">
                メーカー名
            </th>
            <th class="col-lg-1">
                品番
            </th>
            <th class="col-lg-1">
                JANコード
            </th>
            <th class="col-lg-4">
                商品名
            </th>
            <th class="col-lg-1">
                定価
            </th>
            <th class="col-lg-1">
                処理ステータス
            </th>
            <th class="col-lg-1">
                出荷指示
                +全て選択
                -選択解除
            </th>
        </tr>
        </thead>
        <tbody id="results">
        </tbody>
    </table>
    <div id="notice"></div>
    <div class="myPagination">
    </div>
    <div id="modalProduct" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="titleModalProduct">Add Product</h4>
                </div>
                <div class="modal-body">
                    <div class="inputProduct form-group">
                        <label id="lb_product_code">品番</label>
                        <input id="input_product_code" placeholder="" required type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_maker_full_nm">メーカー名</label>
                        <input id="input_maker_full_nm" placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_product_jan">JANコード</label>
                        <input id="input_product_jan" placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_product_name">商品名</label>
                        <input id="input_product_name" placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_list_price">定価</label>
                        <input id="input_list_price" placeholder="" type="text" min="0" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label>ステータス</label>
                        <select id="select_cheetah_status" class="form-control">
                            <option value="0">販売</option>
                            <option value="1">欠品</option>
                            <option value="2">廃盤</option>
                        </select>
                    </div>
                    <div class="inputProduct form-group">
                        <label>処理ステータス</label>
                        <select id="select_process_status" class="form-control">
                            <option value="0">Default</option>
                            <option value="1">Waiting</option>
                            <option value="2">Approved</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnSaveProduct" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDeleteProduct" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Product</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">OK</label>
                        <label>Delete this product</label>
                    </div>
                    <div>
                        <label class="label label-warning">Cancel</label>
                        <label>Return </label>
                    </div>
                    <div class="listProductSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnDeleteProduct" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeStatus1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div class="listProductSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeStatus1" data-dismiss="modal">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeStatus2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div class="listProductSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeStatus2" data-dismiss="modal">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeStatus3" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div class="listProductSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeStatus3" data-dismiss="modal">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalImportCSV" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('importFromCSV') }}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Select File .CSV To Import</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-control">
                            <input type="file" id="file"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" data-dismiss="modal" id="btnImportFromCSV">OK
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="modalPermission" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">NOT PERMISSION</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-danger">Not Permission</label>
                        <label>You are not permission</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>
<script>
    var optionUpdateProduct = "";
    var pageSize = 50;
    var data = {
        action: "load",
        pageSize: 50,
        page: 1,
        cheetahStatus0: false,
        cheetahStatus1: false,
        cheetahStatus2: false,
        product_jan: "",
        maker_full_nm: "",
        product_name: "",
        product_code: ""
    };
    var groupRule = [];
    var groupUser = [];
    var listProductSelected = [];
    var idProductEdit = null;
    $(document).ready(function () {
        getGroupUserLogin();
        loadProduct(data, listProductSelected);
        $("#checkboxCheetahStatus0").prop('checked', true);
        $("#checkboxCheetahStatus1").prop('checked', true);
        $("#checkboxCheetahStatus2").prop('checked', true);
        $("#selectPageSize").change(function () {
            data.pageSize = $(this).val();
            loadProduct(data, listProductSelected);
        });
        $('#tableProduct').on('click', 'input[type="checkbox"]', function () {
            $(".menu-content").css("display", "none");
            var id = $(this).attr('id');
            var pos = listProductSelected.indexOf(id);
            if (pos == -1) {
                listProductSelected.push(id);
            }
            else {
                listProductSelected.splice(pos, 1);
            }
        });
        $('#tableProduct').on('click', 'thead', function () {
            $(".menu-content").css("display", "none");
        });
        $("#results").on("click", '.btnMenu', function () {
            var id = $(this).attr('data-id');
            idProductEdit = id;
            $(".menu-content").css("display", "none");
            var item = $('[data-items-id=' + id + ']');
            item.css('display', 'block');
        });
        $("#results").on("click", ".btnEdit", function () {
            var product = getProductByCode(idProductEdit);
            getProductToEdit(product);
            $("#titleModalProduct").html("Edit Product");
            $('#input_product_code').prop('readonly', true);
            $(".menu-content").css("display", "none");
            $('#modalProduct').modal('toggle');
            optionUpdateProduct = "update";
        });
        $("#results").on("click", ".btnModalDelete", function () {
            $('.listProductSelected').html("");
            var rs = '<label class="labelProductSelected">';
            rs += idProductEdit;
            rs += '</label>';
            $('.listProductSelected').append(rs);
            $(".menu-content").css("display", "none");
            $('#modalDeleteProduct').modal('toggle');
        });
        $("#btnDeleteProduct").click(function () {
            var product = getProductByCode(idProductEdit);
            getProductToEdit(product);
            optionUpdateProduct = "delete";
            updateProduct(product, optionUpdateProduct);
            loadProduct(data, listProductSelected);
        });
        $("#results").on("click", ".btnCancel", function () {
            $(".menu-content").css("display", "none");
        });
        $('#modalProduct').on('hidden.bs.modal', function () {
            if (optionUpdateProduct = "insert") {
                loadProduct(data, listProductSelected);
            }
            refreshModalProduct();
        })
        $("#btnAddProduct").click(function () {
            $(".menu-content").css("display", "none");
            refreshModalProduct();
            $("#titleModalProduct").html("Add Product");
            $('#input_product_code').prop('readonly', false);
            $('#modalProduct').modal('toggle');
            optionUpdateProduct = "insert";
        });
        $("#btnSaveProduct").click(function () {
            if (optionUpdateProduct == "insert") {
                if (!checkValidate()) {
                    return;
                }
                var product = {
                    product_code: $('#input_product_code').val(),
                    product_jan: $('#input_product_jan').val(),
                    product_name: $('#input_product_name').val(),
                    maker_full_nm: $('#input_maker_full_nm').val(),
                    list_price: $('#input_list_price').val(),
                    cheetah_status: $("#select_cheetah_status option:selected").val(),
                    process_status: $("#select_process_status option:selected").val()
                };
                updateProduct(product, optionUpdateProduct);
                refreshModalProduct();
            }
            if (optionUpdateProduct == "update") {
                if (!checkValidate()) {
                    return;
                }
                var product = {
                    product_code: $('#input_product_code').val(),
                    product_jan: $('#input_product_jan').val(),
                    product_name: $('#input_product_name').val(),
                    maker_full_nm: $('#input_maker_full_nm').val(),
                    list_price: $('#input_list_price').val(),
                    cheetah_status: $("#select_cheetah_status option:selected").val(),
                    process_status: $("#select_process_status option:selected").val()
                };
                updateProduct(product, optionUpdateProduct);
                $('#modalProduct').modal('toggle');
                loadProduct(data, listProductSelected);
            }
        });
        $('#input_product_code').change(function () {
            var product_code = $('#input_product_code').val();
            var product = getProductByCode(product_code);
            //if product exist
            if (product != null) {
                $("#lb_product_code").html("品番 is exist");
                $("#lb_product_code").css("color", "red");
                $("#input_product_code").css("border-color", "red");
                $("#btnSaveProduct").prop('disabled', true);
            }
            else {
                $("#lb_product_code").html("品番");
                $("#lb_product_code").css("color", "");
                $("#input_product_code").css("border-color", "");
                $("#btnSaveProduct").prop('disabled', false);
            }
        });
        $('#input_list_price').change(function () {
            var list_price = $('#input_list_price').val();
            if (isNaN(list_price)) {
                $("#lb_list_price").html("定価 is not number");
                $("#lb_list_price").css("color", "red");
                $("#input_list_price").css("border-color", "red");
                $("#btnSaveProduct").prop('disabled', true);
            } else {
                $("#lb_list_price").html("定価");
                $("#lb_list_price").css("color", "");
                $("#input_list_price").css("border-color", "");
                $("#btnSaveProduct").prop('disabled', false);
            }
        });
        $("#btnSearch").click(function () {
            $(".menu-content").css("display", "none");
            if ($("#checkboxCheetahStatus0").prop("checked"))
                data.cheetahStatus0 = true;
            else
                data.cheetahStatus0 = false;
            if ($("#checkboxCheetahStatus1").prop("checked"))
                data.cheetahStatus1 = true;
            else
                data.cheetahStatus1 = false;
            if ($("#checkboxCheetahStatus2").prop("checked"))
                data.cheetahStatus2 = true;
            else
                data.cheetahStatus2 = false;
            data.product_jan = $("#txtProductJan").val();
            data.maker_full_nm = $("#txtMakerFullNm").val();
            data.product_code = $("#txtProductCode").val();
            data.product_name = $("#txtProductName").val();
            data.action = "search";
            data.page = 1;
            loadProduct(data, listProductSelected);
        });
        $("#btnCancel").click(function () {
            pageSize = 50;
            data = {
                action: "load",
                pageSize: 50,
                page: 1,
                cheetahStatus0: false,
                cheetahStatus1: false,
                cheetahStatus2: false,
                product_jan: "",
                maker_full_nm: "",
                product_name: "",
                product_code: ""
            };
            $(".menu-content").css("display", "none");
            $("#checkboxCheetahStatus0").prop('checked', true);
            $("#checkboxCheetahStatus1").prop('checked', true);
            $("#checkboxCheetahStatus2").prop('checked', true);
            $("#txtProductJan").val("");
            $("#txtMakerFullNm").val("");
            $("#txtProductCode").val("");
            $("#txtProductName").val("");
            $("#selectPageSize").val(50).change();
        });
        $("#btnModalChangeStatus1").click(function () {
            $(".menu-content").css("display", "none");
            $('.listProductSelected').html("");
            var i = 0;
            for (i = 0; i < listProductSelected.length; i++) {
                var rs = '<label class="labelProductSelected">';
                rs += listProductSelected[i];
                rs += '</label>';
                $('.listProductSelected').append(rs);
            }
        });
        $("#btnModalChangeStatus2").click(function () {
            $(".menu-content").css("display", "none");
            $('.listProductSelected').html("");
            var i = 0;
            for (i = 0; i < listProductSelected.length; i++) {
                var rs = '<label class="labelProductSelected">';
                rs += listProductSelected[i];
                rs += '</label>';
                $('.listProductSelected').append(rs);
            }
        });
        $("#btnModalChangeStatus3").click(function () {
            $(".menu-content").css("display", "none");
            $('.listProductSelected').html("");
            var i = 0;
            for (i = 0; i < listProductSelected.length; i++) {
                var rs = '<label class="labelProductSelected">';
                rs += listProductSelected[i];
                rs += '</label>';
                $('.listProductSelected').append(rs);
            }
        });
        $("#btnChangeStatus1").click(function () {
            if (listProductSelected.length == 0) {
                return;
            }
            var option = 1;
            changeStatusProduct(listProductSelected, option);
            listProductSelected = [];
            loadProduct(data, listProductSelected);
        });
        $("#btnChangeStatus2").click(function () {
            if (listProductSelected.length == 0) {
                return;
            }
            var option = 2;
            changeStatusProduct(listProductSelected, option);
            listProductSelected = [];
            loadProduct(data, listProductSelected);
        });
        $("#btnChangeStatus3").click(function () {
            if (listProductSelected.length == 0) {
                return;
            }
            var option = 3;
            changeStatusProduct(listProductSelected, option);
            listProductSelected = [];
            loadProduct(data, listProductSelected);
        });
        $("#btnImportFromCSV").click(function () {
            $(".menu-content").css("display", "none");
            var fileName = $('#file').val();
            var typeFile = getExtension(fileName);
            if (isCSV(typeFile)) {
                importFromCSV();
            }
            else {
                alert("Incorrect file format, please try again");
            }
        });
    });
    function loadProduct(data, listProductSelected) {
        //Show image loading when load data
        if ($(".imgLoading").css("display") == "none") {
            $(".imgLoading").show();
        }
        var urlLoad = "";
        if (data.action == "load") {
            for (var i = 0; i < groupRule.length; i++) {
                if (groupRule[i].rule_name == "product-load") {
                    urlLoad = groupRule[i].url;
                }
            }
        }
        if (data.action == "search") {
            for (var i = 0; i < groupRule.length; i++) {
                if (groupRule[i].rule_name == "product-search") {
                    urlLoad = groupRule[i].url;
                }
            }
        }
        if (urlLoad == "") {
            var rs = '<div align="center">Empty Data</div>';
            $("#notice").html(rs);
            $("#lbFrom").text(0);
            $("#lbTo").text(0);
            $("#lbTotal").text(0);
            //Hide image loading
            $(".imgLoading").hide();
            var url = 'product?page=' + urlPage;
            window.history.pushState({path: url}, '', url);
            return;
        }
        urlLoad = '{{url('/')}}' + urlLoad;
        var urlPage = data.page;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlLoad,
            method: "post",
            data: {
                pageNo: data.page,
                pageSize: data.pageSize,
                cheetahStatus0: data.cheetahStatus0,
                cheetahStatus1: data.cheetahStatus1,
                cheetahStatus2: data.cheetahStatus2,
                product_jan: data.product_jan,
                product_code: data.product_code,
                maker_full_nm: data.maker_full_nm,
                product_name: data.product_name,
                _token: _token
            },
            success: function (data) {
                $(".myPagination").html(data.paginate);
                $("#results").html("");
                $("#notice").html("");
                var listProduct = data.data.data;
                var countTotal = data.data.total;
                var countFrom = data.data.from;
                var countTo = data.data.to;
                var i = 0;
                for (i = 0; i < listProduct.length; i++) {
                    var rs = '<tr>';
                    if (listProduct[i].cheetah_status == 0)
                        rs += '<td><button class="btnMenu" data-id="' + listProduct[i].product_code + '"><img src="{{ asset('/img/icon_edit.svg') }}"></button><label class="lbStt label label-success" >販売</label>' +
                        '<div>' +
                        '<div class="menu-content" data-items-id="' + listProduct[i].product_code + '">' +
                        '<button class="btnEdit">Edit</button>' +
                        '</br>' +
                        '<button class="btnModalDelete">Delete</button>' +
                        '</br>' +
                        '<button class="btnCancel">Cancel</button>' +
                        '</div>' +
                        '</div></td>';
                    else if (listProduct[i].cheetah_status == 1)
                        rs += '<td><button class="btnMenu" data-id="' + listProduct[i].product_code + '"><img src="{{ asset('/img/icon_edit.svg') }}"></button><label class="lbStt label label-warning" >欠品</label>' +
                        '<div>' +
                        '<div class="menu-content"  data-items-id="' + listProduct[i].product_code + '">' +
                        '<button class="btnEdit">Edit</button>' +
                        '</br>' +
                        '<button class="btnModalDelete">Delete</button>' +
                        '</br>' +
                        '<button class="btnCancel">Cancel</button>' +
                        '</div>' +
                        '</div></td>';
                    else if (listProduct[i].cheetah_status == 2)
                        rs += '<td><button class="btnMenu" data-id="' + listProduct[i].product_code + '"><img src="{{ asset('/img/icon_edit.svg') }}"></button><label class="lbStt label label-danger" >廃盤</label>' +
                        '<div' +
                        '<div class="menu-content"  data-items-id="' + listProduct[i].product_code + '">' +
                        '<button class="btnEdit">Edit</button>' +
                        '</br>' +
                        '<button class="btnModalDelete">Delete</button>' +
                        '</br>' +
                        '<button class="btnCancel">Cancel</button>' +
                        '</div>' +
                        '</div></td>';
                    if (listProduct[i].maker_full_nm == null)
                        rs += '<td></td>';
                    else
                        rs += '<td>' + listProduct[i].maker_full_nm + '</td>';
                    if (listProduct[i].product_code == null)
                        rs += '<td></td>';
                    else
                        rs += '<td>' + listProduct[i].product_code + '</td>';
                    if (listProduct[i].product_jan == null)
                        rs += '<td></td>';
                    else
                        rs += '<td>' + listProduct[i].product_jan + '</td>';
                    if (listProduct[i].product_name == null)
                        rs += '<td></td>';
                    else
                        rs += '<td>' + listProduct[i].product_name + '</td>';
                    if (listProduct[i].list_price == null)
                        rs += '<td></td>';
                    else
                        rs += '<td>' + listProduct[i].list_price + '</td>';
                    if (listProduct[i].process_status == 0)
                        rs += '<td align="center"><label class="label label-default" >Default</label></td>';
                    else if (listProduct[i].process_status == 1)
                        rs += '<td align="center"><label class="label label-warning" >Waiting</label></td>';
                    else if (listProduct[i].process_status == 2)
                        rs += '<td align="center"><label class="label label-success" >Approved</label></td>';
                    var pos = listProductSelected.indexOf(listProduct[i].product_code);
                    if (pos == -1) {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" id="' + listProduct[i].product_code + '"/></td>';
                    }
                    else {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" checked = "true" id="' + listProduct[i].product_code + '"/></td>';
                    }
                    rs += '</tr>';
                    $("#results").append(rs);
                }
                if (listProduct.length == 0) {
                    var rs = '<div align="center">Empty Data</div>';
                    $("#notice").html(rs);
                    countFrom = 0;
                    countTo = 0;
                }
                $("#lbFrom").text(countFrom);
                $("#lbTo").text(countTo);
                $("#lbTotal").text(countTotal);
                //Hide image loading
                $(".imgLoading").hide();
                var url = 'product?page=' + urlPage;
                window.history.pushState({path: url}, '', url);
            }
        });
    }
    function changeStatusProduct(listProductSelected, option) {
        var urlChangeStatus = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "product-change-status") {
                urlChangeStatus = groupRule[i].url;
            }
        }
        if (urlChangeStatus == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlChangeStatus = '{{url('/')}}' + urlChangeStatus;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlChangeStatus,
            method: "post",
            async: false,
            data: {
                listProductSelected: listProductSelected,
                option: option,
                _token: _token
            },
            success: function () {
                listProductSelected = [];
                loadProduct(data, listProductSelected);
            }
        });
    }
    function getProductByCode(product_code) {
        var product = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('getProductByCode') }}",
            method: "post",
            async: false,
            data: {
                product_code: product_code,
                _token: _token
            },
            success: function (data) {
                product = data.mstProduct[0];
            }
        });
        return product;
    }
    function getProductToEdit(product) {
        $("#input_product_code").val(product.product_code);
        $("#input_product_jan").val(product.product_jan);
        $("#input_product_name").val(product.product_name);
        $("#input_maker_full_nm").val(product.maker_full_nm);
        $("#input_list_price").val(product.list_price);
        if (product.cheetah_status == 0) {
            $('#select_cheetah_status').val('0').change();
        }
        else if (product.cheetah_status == 1) {
            $('#select_cheetah_status').val('1').change();
        }
        else if (product.cheetah_status == 2) {
            $('#select_cheetah_status').val('2').change();
        }
        if (product.process_status == 0) {
            $('#select_process_status').val('0').change();
        }
        else if (product.process_status == 1) {
            $('#select_process_status').val('1').change();
        }
        else if (product.process_status == 2) {
            $('#select_process_status').val('2').change();
        }
    }
    function updateProduct(product, optionUpdateProduct) {
        var urlUpdateProduct = "";
        if (optionUpdateProduct == "insert") {
            for (var i = 0; i < groupRule.length; i++) {
                if (groupRule[i].rule_name == "product-insert") {
                    urlUpdateProduct = groupRule[i].url;
                }
            }
        }
        if (optionUpdateProduct == "update") {
            for (var i = 0; i < groupRule.length; i++) {
                if (groupRule[i].rule_name == "product-update") {
                    urlUpdateProduct = groupRule[i].url;
                }
            }
        }
        if (optionUpdateProduct == "delete") {
            for (var i = 0; i < groupRule.length; i++) {
                if (groupRule[i].rule_name == "product-delete") {
                    urlUpdateProduct = groupRule[i].url;
                }
            }
        }
        if (urlUpdateProduct == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlUpdateProduct = '{{url('/')}}' + urlUpdateProduct;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlUpdateProduct,
            method: "post",
            async: false,
            data: {
                product: product,
                _token: _token
            },
            success: function () {
                if (optionUpdateProduct == "insert" || optionUpdateProduct == "update") {
                    alert("Success");
                }
            },
            error: function () {
                alert("Error process");
            }
        });
    }
    function refreshModalProduct() {
        $("#lb_product_code").html("品番");
        $("#lb_product_code").css("color", "");
        $("#input_product_code").css("border-color", "");
        $("#lb_list_price").html("定価");
        $("#lb_list_price").css("color", "");
        $("#input_list_price").css("border-color", "");
        $("#btnSaveProduct").prop('disabled', false);

        $("#input_product_code").val("");
        $("#input_product_jan").val("");
        $("#input_product_name").val("");
        $("#input_maker_full_nm").val("");
        $("#input_list_price").val("");
        $('#select_cheetah_status').val('0').change();
        $('#select_process_status').val('0').change();
    }
    function checkValidate() {
        if ($("#input_product_code").val() == "") {
            $("#lb_product_code").html("品番 is empty");
            $("#lb_product_code").css("color", "red");
            $("#input_product_code").css("border-color", "red");
            return false;
        }
        else {
            $("#lb_product_code").html("品番");
            $("#lb_product_code").css("color", "");
            $("#input_product_code").css("border-color", "");
        }
        if (isNaN($("#input_list_price").val())) {
            $("#lb_list_price").html("定価 is not number");
            $("#lb_list_price").css("color", "red");
            $("#input_list_price").css("border-color", "red");
            return false;
        } else {
            $("#lb_list_price").html("定価");
            $("#lb_list_price").css("color", "");
            $("#input_list_price").css("border-color", "");
        }
        return true;
    }
    function getGroupUserLogin() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{url('/user/get-group')}}',
            method: "post",
            async: false,
            success: function (data) {
                groupUser = data.groupUser;
                groupRule = data.groupRule;
            }
        });
    }
    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }
    function isCSV(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'csv':
                return true;
        }
        return false;
    }
    function importFromCSV() {
        //Show image loading when import data
        if ($(".imgLoading").css("display") == "none") {
            $(".imgLoading").show();
        }
        var formData = new FormData();
        formData.append('selectFile', $('#file')[0].files[0]);
        $.ajax({
            url: "{{ route('importFromCSV') }}",
            type: 'POST',
            data: formData,
            dataType: "json",
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                $('#file').val("");
                //Hide image loading
                $(".imgLoading").hide();
                if (data.flag == true) {
                    alert("Import Complete, please refresh page!");
                }
                else {
                    alert("Error, please try again!");
                }
            }
        });
    }
    function checkAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = true;
            var pos = listProductSelected.indexOf(listCheckBox[i].id);
            if (pos == -1) {
                listProductSelected.push(listCheckBox[i].id);
            }
        }
    }
    function UnCheckAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = false;
            var pos = listProductSelected.indexOf(listCheckBox[i].id);
            if (pos != -1) {
                listProductSelected.splice(pos, 1);
            }
        }
    }
</script>
