{{--
 * Created by PhpStorm.
 * User: tran.tuan@rivercrance.com
 * Date: 6/13/2019
 * Time: 8:58 AM--}}
@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div>
        <button id="btnAddUser" class="btn btn-primary">Add User</button>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <div id="formInput">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <input type="checkbox" name="ckbActivate" id="ckbActivate">Activate
        <input type="checkbox" name="ckbDeactivate" id="ckbDeactivate">Deactivate
        <label>Admin Id</label>
        <input type="text" name="t_admin_id" id="t_admin_id" value=""/>
        <label>Email</label>
        <input type="text" name="email" id="email" value=""/>
        <input class="btn btn-primary" name="btnSearch" id="btnSearch" type="button" value="Search"/>
        <input class="btn btn-primary" name="btnCancel" id="btnCancel" type="button" value="Cancel"/>
    </div>
    <div class="groupBtnChangeStatus">
        <a href="#" id="btnModalActivate" class="btn btn-success" data-toggle="modal" data-target="#modalChangeStatus">Activate</a>
        <a href="#" id="btnModalDeactivate" class="btn btn-danger" data-toggle="modal" data-target="#modalChangeStatus">Deactivate</a>
        <a href="#" id="btnModalChangeGroup" class="btn btn-primary">Change Group User</a>
        <a href="{{url('group-user')}}" id="btnManageGroupUser" class="btn btn-warning">Manage Group User</a>
    </div>
    <div class="labelCount">
        <select id="selectPageSize">
            <option value="50" selected="selected">50</option>
            <option value="100">100</option>
            <option value="250">250</option>
            <option value="500">500</option>
        </select>
        <label>Từ </label>
        <label id="lbFrom"></label>
        <label> Đến </label>
        <label id="lbTo"></label>
        <label> Trong </label>
        <label id="lbTotal"></label>
        <label> Kết quả</label>
    </div>
    <div class="checkAllCheckBox">
        <button id="btnCheck" onclick="checkAll()" class="btn btn-success">+全て選択</button>
        <button id="btnUnCheck" onclick="UnCheckAll()" class="btn btn-danger">-選択解除</button>
    </div>
    <div class="myPagination">
    </div>
    <div class="imgLoading">
        <img src="{{ asset('/img/loading.gif') }}" alt="">
    </div>
    <table id="tableUser">
        <thead>
        <tr>
            <th class="col-lg-2">
                Admin ID
            </th>
            <th class="col-lg-4">
                Email
            </th>
            <th class="col-lg-2">
                Status
            </th>
            <th class="col-lg-2">
                Group User
            </th>
            <th class="col-lg-1">
                Last IP Login
            </th>
            <th class="col-lg-1">
                出荷指示
                +全て選択
                -選択解除
            </th>
        </tr>
        </thead>
        <tbody id="resultsUser">
        </tbody>
    </table>
    <div id="notice"></div>
    <div class="myPagination"></div>
    <div id="modalUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="titleModalUser">Add User</h4>
                </div>
                <div class="modal-body">
                    <div class="inputProduct form-group" hidden>
                        <label id="lb_t_admin_id">Admin ID</label>
                        <input id="input_t_admin_id" readonly placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_email">Email</label>
                        <input id="input_email" placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="inputProduct form-group">
                        <label id="lb_password">Password</label>
                        <input id="input_password" placeholder="" type="password" class="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnSaveUser" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDeleteUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete User</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">OK</label>
                        <label>Delete this user</label>
                    </div>
                    <div>
                        <label class="label label-warning">Cancel</label>
                        <label>Return </label>
                    </div>
                    <div class="listUserSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnDeleteUser" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeStatus" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div class="listUserSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeStatus" data-dismiss="modal">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeGroup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Group User</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div>
                        <label>Select Group User</label>
                        <select id="select_group_user" class="form-control"></select>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="listUserSelected"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeGroupUser" data-dismiss="modal">確定
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPermission" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">NOT PERMISSION</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-danger">Not Permission</label>
                        <label>You are not permission</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
<script>
    var optionUpdateUser = "";
    var optionChangeStatus = "";
    var flagChangePassword = false;
    var action = "load";
    var data = {
        action: action,
        pageSize: 50,
        page: 1,
        t_admin_id: "",
        email: "",
        activate: true,
        deactivate: true
    };
    var listGroupUser = [];
    var listUserSelected = [];
    var idUserEdit = null;
    var groupRule = [];
    var groupUser = [];
    $(document).ready(function () {
        getGroupUserLogin();
        var flag = false;
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "user-load") {
                flag = true;
            }
        }
        if (flag == false) {
            $('#modalPermission').modal('toggle');
            var url = '{{url('/')}}';
            $(location).attr('href', url);
            return;
        }
        getAllGroupUser();
        loadUser(data, listUserSelected);
        $("#ckbActivate").prop('checked', true);
        $("#ckbDeactivate").prop('checked', true);
        $("#group_name_user_login").html(groupUser.group_name);
        $("#group_comment_user_login").html(groupUser.group_comment);
        $('#tableUser').on('click', 'input[type="checkbox"]', function () {
            $(".menuUser").css("display", "none");
            var id = $(this).attr('data-id');
            var pos = listUserSelected.indexOf(id);
            if (pos == -1) {
                listUserSelected.push(id);
            }
            else {
                listUserSelected.splice(pos, 1);
            }
        });
        $("#resultsUser").on("click", '.btnMenu', function () {
            var id = $(this).attr('data-id');
            idUserEdit = id;
            $(".menuUser").css("display", "none");
            var item = $('[menu-id=' + id + ']');
            item.css('display', 'block');
        });
        $("#resultsUser").on("click", ".btnCancel", function () {
            $(".menuUser").css("display", "none");
        });
        $("#btnSearch").click(function () {
            $(".menuUser").css("display", "none");
            if ($("#ckbActivate").prop("checked"))
                data.activate = true;
            else
                data.activate = false;
            if ($("#ckbDeactivate").prop("checked"))
                data.deactivate = true;
            else
                data.deactivate = false;
            data.t_admin_id = $("#t_admin_id").val();
            data.email = $("#email").val();
            data.page = 1;
            data.action = "search";
            loadUser(data, listUserSelected);
        });
        $("#btnCancel").click(function () {
            action = "load";
            data = {
                action: action,
                pageSize: 50,
                page: 1,
                t_admin_id: "",
                email: "",
                activate: true,
                deactivate: true
            };
            $(".menuUser").css("display", "none");
            $("#ckbActivate").prop('checked', true);
            $("#ckbDeactivate").prop('checked', true);
            $("#t_admin_id").val("");
            $("#email").val("");
            listUserSelected = [];
            $("#selectPageSize").val(50).change();
        });
        $("#selectPageSize").change(function () {
            data.pageSize = $(this).val();
            loadUser(data, listUserSelected);
        });
        $("#btnModalActivate").click(function () {
            $(".menuUser").css("display", "none");
            $('.listUserSelected').html("");
            optionChangeStatus = "active";
            var i = 0;
            for (i = 0; i < listUserSelected.length; i++) {
                var rs = '<label class="labelUserSelected">';
                rs += listUserSelected[i];
                rs += '</label>';
                $('.listUserSelected').append(rs);
            }
        });
        $("#btnModalDeactivate").click(function () {
            $(".menuUser").css("display", "none");
            $('.listUserSelected').html("");
            optionChangeStatus = "deactive";
            var i = 0;
            for (i = 0; i < listUserSelected.length; i++) {
                var rs = '<label class="labelUserSelected">';
                rs += listUserSelected[i];
                rs += '</label>';
                $('.listUserSelected').append(rs);
            }
        });
        $("#btnChangeStatus").click(function () {
            if (listUserSelected.length == 0) {
                return;
            }
            changeStatusUser(listUserSelected, optionChangeStatus);
            listUserSelected = [];
            optionChangeStatus = "";
            loadUser(data, listUserSelected);
        });
        $("#btnModalChangeGroup").click(function () {
            $(".menuUser").css("display", "none");
            $('.listUserSelected').html("");
            $('#select_group_user option').remove()
            var i;
            for (i = 0; i < listGroupUser.length; i++) {
                $('#select_group_user').append($('<option>', {
                    value: listGroupUser[i].group_id,
                    text: listGroupUser[i].group_name
                }));
            }
            for (i = 0; i < listUserSelected.length; i++) {
                var rs = '<label class="labelUserSelected">';
                rs += listUserSelected[i];
                rs += '</label>';
                $('.listUserSelected').append(rs);
            }
            $('#modalChangeGroup').modal('toggle');
        });
        $("#btnChangeGroupUser").click(function () {
            if (listUserSelected.length == 0) {
                return;
            }
            var groupId = $('#select_group_user').val();
            changeGroupUser(listUserSelected, groupId);
            listUserSelected = [];
            optionChangeStatus = "";
            loadUser(data, listUserSelected);
        });
        $('#modalUser').on('hidden.bs.modal', function () {
            if (optionUpdateUser = "insert") {
                loadUser(data, listUserSelected);
            }
            refreshModalUser();
        })
        $("#btnAddUser").click(function () {
            $(".menuUser").css("display", "none");
            refreshModalUser();
            $("#titleModalUser").html("Add User");
            optionUpdateUser = "insert";
            $('#modalUser').modal('toggle');
        });
        $("#resultsUser").on("click", ".btnEdit", function () {
            var user = getUserById(idUserEdit);
            getUserToEdit(user);
            $("#titleModalUser").html("Edit User");
            $(".menuUser").css("display", "none");
            optionUpdateUser = "update";
            $('#modalUser').modal('toggle');
        });
        $("#resultsUser").on("click", ".btbDelete", function () {
            $('.listUserSelected').html("");
            var rs = '<label class="labelUserSelected">';
            rs += idUserEdit;
            rs += '</label>';
            $('.listUserSelected').append(rs);
            $(".menuUser").css("display", "none");
            optionUpdateUser = "delete";
            $('#modalDeleteUser').modal('toggle');
        });
        $("#btnDeleteUser").click(function () {
            var user = {
                t_admin_id: idUserEdit,
                optionUpdateUser: optionUpdateUser
            };
            optionUpdateUser = "delete";
            updateUser(user, optionUpdateUser);
            loadUser(data, listUserSelected);
        });
        $("#btnSaveUser").click(function () {
            if (optionUpdateUser == "insert") {
                var user = {
                    t_admin_id: null,
                    email: $('#input_email').val(),
                    password: $('#input_password').val(),
                    optionUpdateUser: optionUpdateUser
                };
                updateUser(user, optionUpdateUser);
            }
            if (optionUpdateUser == "update") {
                var user = {
                    flagChangePassword: flagChangePassword,
                    t_admin_id: idUserEdit,
                    email: $('#input_email').val(),
                    password: $('#input_password').val(),
                    optionUpdateUser: optionUpdateUser
                };
                updateUser(user, optionUpdateUser);
                //$('#modalUser').modal('toggle');
                loadUser(data, listUserSelected);
            }
        });
        $('#input_email').change(function () {
            var email = $('#input_email').val();
            var user = getUserByEmail(email);
            //if product exist
            if (user != null) {
                $("#lb_email").html("Email is exist");
                $("#lb_email").css("color", "red");
                $("#input_email").css("border-color", "red");
                $("#btnSaveUser").prop('disabled', true);
            }
            else {
                $("#lb_email").html("Email");
                $("#lb_email").css("color", "");
                $("#input_email").css("border-color", "");
                $("#btnSaveUser").prop('disabled', false);
            }
        });
        $('#input_password').change(function () {
            flagChangePassword = true;
        });
    });
    function loadUser(data, listUserSelected) {
        //Show image loading when load data
        if ($(".imgLoading").css("display") == "none") {
            $(".imgLoading").show();
        }
        var urlLoad = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "user-load") {
                urlLoad = groupRule[i].url;
            }
        }
        if (urlLoad == "") {
            var rs = '<div align="center">Empty Data</div>';
            $("#notice").html(rs);
            $("#lbFrom").text(0);
            $("#lbTo").text(0);
            $("#lbTotal").text(0);
            //Hide image loading
            $(".imgLoading").hide();
            var url = 'user?page=' + urlPage;
            window.history.pushState({path: url}, '', url);
            return;
        }
        urlLoad = '{{url('/')}}' + urlLoad;
        var urlPage = data.page;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlLoad,
            method: "post",
            data: {
                data: data,
                _token: _token
            },
            success: function (data) {
                $(".myPagination").html(data.paginate);
                $("#resultsUser").html("");
                $("#notice").html("");
                var listUser = data.data.data;
                var countTotal = data.data.total;
                var countFrom = data.data.from;
                var countTo = data.data.to;
                var disabled = "disabled";
                var i = 0;
                for (i = 0; i < listUser.length; i++) {
                    var rs = '<tr>';
                    rs += '<td><button class="btnMenu" data-id="' + listUser[i].t_admin_id + '"><img src="{{ asset('/img/icon_edit.svg') }}"></button><label class="lb_t_admin_id">' + listUser[i].t_admin_id + '</label>' +
                    '<div class="menuUser" menu-id="' + listUser[i].t_admin_id + '">' +
                    '<div class="menuUser-content">' +
                    '<div class="itemMenu"><a class="btnEdit" href="#">Edit</a></div>' +
                    '<div class="itemMenu"><a class="btbDelete" href="#">Delete</a></div>' +
                    '<div class="itemMenu"><a class="btnCancel" href="#">Cancel</a></div>' +
                    '</div>' +
                    '</div>' +
                    '</td>';
                    rs += '<td>' + listUser[i].email + '</td>';
                    if (listUser[i].is_active == 0)
                        rs += '<td align="center"><label class="label label-danger" >Deactivate</label></td>';
                    else if (listUser[i].is_active == 1)
                        rs += '<td align="center"><label class="label label-success" >Activate</label></td>';
                    var color = ""
                    if (listUser[i].group_id == 1) {
                        color = "label label-danger";
                    }
                    else if (listUser[i].group_id == 2) {
                        color = "label label-primary";
                    }
                    else if (listUser[i].group_id == 3) {
                        color = "label label-success";
                    }
                    else {
                        color = "label label-default";
                    }
                    rs += '<td align="center"><label class="' + color + '">' + listUser[i].group_name + '</label></td>';
                    if (listUser[i].last_login_ip == null)
                        rs += '<td></td>';
                    else
                        rs += '<td align="center">' + listUser[i].last_login_ip + '</td>';
                    var pos = listUserSelected.indexOf(listUser[i].t_admin_id.toString());
                    if (pos == -1) {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" data-id="' + listUser[i].t_admin_id + '"/></td>';
                    }
                    else {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" checked = "true" data-id="' + listUser[i].t_admin_id + '"/></td>';
                    }
                    rs += '</tr>';
                    $("#resultsUser").append(rs);
                }
                if (listUser.length == 0) {
                    var rs = '<div align="center">Empty Data</div>';
                    $("#notice").html(rs);
                    countFrom = 0;
                    countTo = 0;
                }
                $("#lbFrom").text(countFrom);
                $("#lbTo").text(countTo);
                $("#lbTotal").text(countTotal);
                //Hide image loading
                $(".imgLoading").hide();
                var url = 'user?page=' + urlPage;
                window.history.pushState({path: url}, '', url);
            }
        });
    }
    function changeStatusUser(listUserSelected, option) {
        var urlChangeStatus = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "user-active") {
                urlChangeStatus = groupRule[i].url;
            }
        }
        if (urlChangeStatus == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlChangeStatus = '{{url('/')}}' + urlChangeStatus;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlChangeStatus,
            method: "post",
            async: false,
            data: {
                listUserSelected: listUserSelected,
                option: option,
                _token: _token
            },
            success: function () {
            }
        });
    }
    function changeGroupUser(listUserSelected, groupId) {
        var urlChangeGroup = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "user-change-group") {
                urlChangeGroup = groupRule[i].url;
            }
        }
        if (urlChangeGroup == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlChangeGroup = '{{url('/')}}' + urlChangeGroup;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlChangeGroup,
            method: "post",
            async: false,
            data: {
                listUserSelected: listUserSelected,
                groupId: groupId,
                _token: _token
            },
            success: function () {
            }
        });
    }
    function updateUser(user) {
        var urlUpdateUser = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "user-update") {
                urlUpdateUser = groupRule[i].url;
            }
        }
        if (urlUpdateUser == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlUpdateUser = '{{url('/')}}' + urlUpdateUser;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlUpdateUser,
            method: "post",
            async: false,
            data: {
                user: user,
                _token: _token
            },
            success: function (data) {
                if (data.email == "" && data.email == "") {
                    $('#modalUser').modal('toggle');
                }
                if (data.email != "") {
                    $("#lb_email").html("Email: " + data.email);
                    $("#lb_email").css("color", "red");
                    $("#input_email").css("border-color", "red");
                }
                else {
                    $("#lb_email").html("Email");
                    $("#lb_email").css("color", "");
                    $("#input_email").css("border-color", "");
                }
                if (data.password != "") {
                    $("#lb_password").html("Password: " + data.password);
                    $("#lb_password").css("color", "red");
                    $("#input_password").css("border-color", "red");
                }
                else {
                    $("#lb_password").html("Password");
                    $("#lb_password").css("color", "");
                    $("#input_password").css("border-color", "");
                }
            }
        });
    }
    function refreshModalUser() {
        $("#lb_email").html("Email");
        $("#lb_email").css("color", "");
        $("#input_email").css("border-color", "");
        $("#lb_password").html("Password");
        $("#lb_password").css("color", "");
        $("#input_password").css("border-color", "");
        $("#btnSaveProduct").prop('disabled', false);

        $("#input_email").val("");
        $("#input_password").val("");
    }
    function getUserToEdit(user) {
        $("#input_email").val(user.email);
        $("#input_password").val("*********");
    }
    function getGroupUserLogin() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{url('/user/get-group')}}',
            method: "post",
            async: false,
            success: function (data) {
                groupUser = data.groupUser;
                groupRule = data.groupRule;
            }
        });
    }
    function getAllGroupUser() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{route('get-all-group-user')}}',
            method: "post",
            async: false,
            success: function (data) {
                var listGroupUserGet = data.listGroupUser;
                for (var i = 0; i < listGroupUserGet.length; i++) {
                    listGroupUser.push(listGroupUserGet[i]);
                }
            }
        });
    }
    function getUserByEmail(email) {
        var user = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('get-user-by-email') }}",
            method: "post",
            async: false,
            data: {
                email: email,
                _token: _token
            },
            success: function (data) {
                user = data.user[0];
            }
        });
        return user;
    }
    function getUserById(id) {
        var user = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('get-user-by-id') }}",
            method: "post",
            async: false,
            data: {
                t_admin_id: id,
                _token: _token
            },
            success: function (data) {
                user = data.user[0];
            }
        });
        return user;
    }
    function checkAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = true;
            var id = listCheckBox[i].getAttribute("data-id").toString();
            var pos = listUserSelected.indexOf(id);
            if (pos == -1) {
                listUserSelected.push(id);
            }
        }
    }
    function UnCheckAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = false;
            var id = listCheckBox[i].getAttribute("data-id").toString();
            var pos = listUserSelected.indexOf(id);
            if (pos != -1) {
                listUserSelected.splice(pos, 1);
            }
        }
    }
</script>

