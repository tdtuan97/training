@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div style="display: inline-block">
        <h2>｜受注一覧</h2>
        <h4>「GLS大都南港物流センターに発送する受注情報になります。出荷予定日を入力して下さい。」</h4>
    </div>
    <form method="post" action="{{url('order')}}" class="formInput">
        <div>
           {{-- <input type="hidden" name="sort" value="{{$sort}}" id="sort" />--}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label>商品名</label>
            <input id="s_name" name="s_name" value="{{$s_name}}" type="text"/>
            <label>発注コード</label>
            <input id="s_supplieritemcd" name="s_supplieritemcd" value="{{$s_supplieritemcd}}" type="text"/>
            <label>JANコード</label>
            <input id="s_jan_code" name="s_jan_code" value="{{$s_jan_code}}" type="text"/>
            <label>メーカー品番</label>
            <input id="s_maker_code" name="s_maker_code" value="{{$s_maker_code}}" type="text"/>
        </div>
        <div>
            <label>注番</label>
            <input id="s_order_code" name="s_order_code" value="{{$s_order_code}}" type="text"/>
            <label>メモ</label>
            <input id="s_memo" name="s_memo" value="{{$s_memo}}" type="text"/>
            <label>FaxNo</label>
            <input id="s_send_fax_date" name="s_send_fax_date" value="{{$s_send_fax_date}}" type="text"/>
        </div>
        <div>
            <input id="supplykb1" name="supplykb1" type="checkbox"  @if(!empty($supplykb1))
                   checked
                    @endif/>
            <label>客注</label>
            <input id="supplykb2" name="supplykb2" type="checkbox" @if(!empty($supplykb2))
                   checked
                    @endif/>
            <label>月初 |</label>
            <input id="s_m_order_type_id" name="s_m_order_type_id" type="checkbox"/>
            <label>受注</label>
            <input id="s_m_order_type_id" name="s_m_order_type_id" type="checkbox"/>
            <label>欠品</label>
            <input id="s_m_order_type_id" name="s_m_order_type_id" type="checkbox"/>
            <label>廃番</label>
            <input id="s_m_order_type_id" name="s_m_order_type_id" type="checkbox"/>
            <label>受注キャンセル</label>
            <input id="s_m_order_type_id" name="s_m_order_type_id" type="checkbox"/>
            <label>発注無効  | </label>
            <input id="s_percent" name="s_percent" type="checkbox"  @if(!empty($s_percent))
                   checked @endif/>
            <label>5％よりも高い商品 |</label>
        </div>
        <div>
            <input id="reminder_flg1" name="reminder_flg1" type="checkbox" @if($reminder_flg1 == 1)
                   checked
                    @endif/>
            <label>出荷予定日を早めにお願い致します</label>
            <input id="reminder_flg2" name="reminder_flg2" type="checkbox" @if($reminder_flg2 == 1)
                   checked
                    @endif/>
            <label>出荷予定日がない場合はキャンセルします</label>
        </div>
        <div class="groupButtonOrder">
            <input class="btn btn-primary" value="検索する" type="submit"/>
            <input type="button" class="btn btn-primary" value="登録する"/>
        </div>
        <div class="groupButtonOrder">
            <input type="button" class="btn btn-primary" value="+1日" id="day_plus" name="day_plus"/>
            <input type="button" class="btn btn-primary" value="-1日" id="day_minus" name="day_minus"/>
            <input type="button" class="btn btn-primary" value="本日の日付を入力する" id="day" name="day"/>
        </div>
        <div class="groupButtonOrder">
            <input type="button" class="btn btn-default" value="ファイルを選択"/>
            <label>選択されていません</label>
            <input type="button" class="btn btn-primary" id="csvUpload" name="csvUpload" value="CSVアップロード"/>
            <input type="button" class="btn btn-primary" id="csvDownload" name="csvDownload" value="CSVダウンロード"/>
            <input type="button" class="btn btn-primary" id="printList" name="printList" value="印刷"/>
        </div>
        <table align="right">
            <tr>
                <td>
                    {{$t_order_data->appends([
                         'order_date_year'         => $order_date_year,
                         'order_date_month'        => $order_date_month,
                         'order_date_day'          => $order_date_day,
                         'order_date_h'            => $order_date_h,
                         'order_date_i'            => $order_date_i,
                         'order_date_s'            => $order_date_s,
                         'order_date_year_to'      => $order_date_year_to,
                         'order_date_month_to'     => $order_date_month_to,
                         'order_date_day_to'       => $order_date_day_to,
                         'order_date_h_to'         => $order_date_h_to,
                         'order_date_i_to'         => $order_date_i_to,
                         'order_date_s_to'         => $order_date_s_to,
                         'supplykb1'               => $supplykb1,
                         'supplykb2'               => $supplykb2,
                         's_name'                  => $s_name,
                         's_supplieritemcd'        => $s_supplieritemcd,
                         's_jan_code'              => $s_jan_code,
                         's_maker_code'            => $s_maker_code,
                         's_order_code'            => $s_order_code,
                         's_memo'                  => $s_memo,
                         's_send_fax_date'         => $s_send_fax_date,
                         'reminder_flg1'           => $reminder_flg1,
                         'reminder_flg2'           => $reminder_flg2,
                         's_m_order_type_id'       => $s_m_order_type_id,
                         's_percent'               => $s_percent,
                         'per_page'                => $per_page,
                         'sort'                    => $sort,
                         ])->render()}}　
                </td>
                <td>
                    <select id="per_page" name="per_page">
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="250">250</option>
                    </select>
                </td>
                <td>
                    全{{$t_order_data->total()}}件&nbsp;&nbsp;
                </td>
            </tr>
        </table>
    </form>
    <table class="tableOrder table table-bordered">
        <tr class="labelTable">
            <th >
                <label>受注日</label>
                <a href="#">▼</a>
                <a href="#">▲</a>
            </th>
            <th>
                <label>発注コード</label>
            </th>
            <th >
                <label>JANコード</label>
                <a href="#">▼</a>
                <a href="#">▲</a>
            </th>
            <th >
                <label>メーカー品番</label>
            </th>
            <th >
                <div>
                    <label>商品番号</label>
                </div>
                <div>
                    <label>商品名</label>
                </div>
                <div>
                    <label>サイズ </label>
                </div>
                <div>
                    <label>色</label>
                </div>
            </th>
            <th>
                <label>納品先</label>
            </th>
            <th >
                <label>価格</label>
            </th>
            <th >
                <label>注文数</label>
            </th>
            <th style="width: 6%">
                <label></label>
            </th>
            <th>
                <label>出荷予定日</label>
            </th>
            <th >
                <label>備考欄(大都へ送信されます)</label>
                <label>メモ(検索用)</label>
            </th>
            <th>
                <label>種別</label>
            </th>
            <th >
                <label>注番</label>
            </th>
            <th style="width: 85px">
                <label>登録</label>
                <br/>
                <a href="#">+全て選択</a>
                <br/>
                <a href="#">-選択解除</a>
            </th>
        </tr>
        @foreach( $t_order_data as $data)
            <tr>
                <td>
                    <a href="#">{{$data->order_date}}</a>
                </td>
                <td>
                    <a href="?s_supplieritemcd={{$data->supplieritemcd}}">{{$data->supplieritemcd}}</a>
                </td>
                <td>
                    <a href="?s_jan_code={{$data->jan_code}}">{{$data->jan_code}}</a>
                </td>
                <td>
                    <label>{{$data->maker_code}}</label>
                </td>
                <td>
                    <label>{{$data->item_code}}</label>
                    <label>{{$data->name}}</label>
                    <label>{{$data->size}}</label>
                    <label>{{$data->color}}</label>
                </td>
                <td>
                    <label>{{$data->delivery}}</label>
                </td>
                <td>
                    <label>{{$data->price}}</label>
                </td>
                <td>
                    <label>{{$data->quantity}}</label>
                </td>
                <td>
                    @foreach($m_order_type_data_radio as $orderTypeRadio)
                        @if($orderTypeRadio->m_order_type_id == $data->m_order_type_id)
                           <input type="radio" checked/>{{$orderTypeRadio->name}}
                            <br/>
                        @else
                            <input type="radio"/>{{$orderTypeRadio->name}}
                            <br/>
                        @endif
                    @endforeach
                </td>
                <td>
                    <input style="width: 100%" type="text"/>
                    <br/>
                    <input id="direct_flg" name="direct_flg" type="checkbox"/>
                    <label style="font-size: 0.7em; font-weight: 500">メーカー直送</label>
                    <input style="width: 100%; height: 50px" type="text"/>
                    <br/>
                    <label style="font-size: 0.7em; font-weight: 500">発送元をご入力下さい</label>
                </td>
                <td>
                    <input style="width: 100%; height: 70px" type="text"/>
                    <input style="width: 100%; height: 70px" type="text"/>
                </td>
                <td>
                    <label>{{$data->supplykb}}</label>
                </td>
                <td>
                    <a href="#">{{$data->order_code}}</a>
                </td>
                <td align="center">
                    <input type="checkbox" data-id="{{$data->t_order_id}}"/>
                </td>
            </tr>
        @endforeach
    </table>
@endsection