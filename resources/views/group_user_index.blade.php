@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
    <div>
        <button id="btnAddGroup" class="btn btn-primary">Add Group User</button>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <div id="formInput">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <label>Group Id</label>
        <input type="text" name="group_id" id="group_id" value=""/>
        <label>Group Name</label>
        <input type="text" name="group_name" id="group_name" value=""/>
        <label>Group Comment</label>
        <input type="text" name="group_comment" id="group_comment" value=""/>
        <input class="btn btn-primary" name="btnSearch" id="btnSearch" type="button" value="Search"/>
        <input class="btn btn-primary" name="btnCancel" id="btnCancel" type="button" value="Cancel"/>
    </div>
    <div class="groupBtnChangeStatus">
        <a href="#" id="btnModalChangeRule" class="btn btn-danger" data-toggle="modal" data-target="#modalChangeRule">Change
            Rule</a>
    </div>
    <div class="labelCount">
        <select id="selectPageSize">
            <option value="50" selected="selected">50</option>
            <option value="100">100</option>
            <option value="250">250</option>
            <option value="500">500</option>
        </select>
        <label>Từ </label>
        <label id="lbFrom"></label>
        <label> Đến </label>
        <label id="lbTo"></label>
        <label> Trong </label>
        <label id="lbTotal"></label>
        <label> Kết quả</label>
    </div>
    <div class="checkAllCheckBox">
        <button id="btnCheck" onclick="checkAll()" class="btn btn-success">+全て選択</button>
        <button id="btnUnCheck" onclick="UnCheckAll()" class="btn btn-danger">-選択解除</button>
    </div>
    <div class="myPagination">
    </div>
    <div class="imgLoading">
        <img src="{{ asset('/img/loading.gif') }}" alt="">
    </div>
    <table id="tableGroup">
        <thead>
        <tr>
            <th class="col-lg-2">
                Group ID
            </th>
            <th class="col-lg-4">
                Group Name
            </th>
            <th class="col-lg-5">
                Group Comment
            </th>
            <th class="col-lg-1">
                出荷指示
                +全て選択
                -選択解除
            </th>
        </tr>
        </thead>
        <tbody id="resultsGroup">
        </tbody>
    </table>
    <div id="notice"></div>
    <div class="myPagination"></div>
    <div id="modalGroup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="titleModalUser">Add Group User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" hidden>
                        <label id="lb_group_id">Group Id</label>
                        <input id="input_group_id" readonly placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label id="lb_group_name">Group Name</label>
                        <input id="input_group_name" placeholder="" type="text" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label id="lb_group_comment">Group Comment</label>
                        <textarea id="input_group_comment" class="form-control"></textarea>
                    </div>
                    <h4>Select Rule Of Group</h4>

                    <div class="listRule"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnSaveGroup" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDeleteGroup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Group User</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">OK</label>
                        <label>Delete this group user</label>
                    </div>
                    <div>
                        <label class="label label-warning">Cancel</label>
                        <label>Return </label>
                    </div>
                    <div class="listUserSelected">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="btnDeleteGroup" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalChangeRule" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">商品</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-primary">廃番</label>
                        <label>下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</label>
                    </div>
                    <div>
                        <label class="label label-warning">閉じる</label>
                        <label>廃番にしない場合は、閉じる ボタンを押してください。</label>
                    </div>
                    <div class="listGroupSelected">
                    </div>
                    <h4>Select Rule To Change</h4>

                    <div class="listRule"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
                    <button type="button" class="btn btn-primary" id="btnChangeRule" data-dismiss="modal">確定</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPermission" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">NOT PERMISSION</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <label class="label label-danger">Not Permission</label>
                        <label>You are not permission</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
<script>
    var optionUpdateGroup = "";
    var optionChangeRule = "";
    var action = "load";
    var data = {
        action: action,
        pageSize: 50,
        page: 1,
        group_id: "",
        group_name: "",
        group_comment: ""
    };
    var listRule = [];
    var listGroupSelected = [];
    var listRuleSelected = [];
    var idGroupEdit = null;
    var groupRule = [];
    var groupUser = [];
    $(document).ready(function () {
        getGroupUserLogin();
        checkPermission();
        loadGroup(data, listGroupSelected);
        getAllRule();
        $('#tableGroup').on('click', 'input[type="checkbox"]', function () {
            $(".menuGroup").css("display", "none");
            var id = $(this).attr('data-id');
            var pos = listGroupSelected.indexOf(id);
            if (pos == -1) {
                listGroupSelected.push(id);
            }
            else {
                listGroupSelected.splice(pos, 1);
            }
        });
        $('.listRule').on('click', 'input[type="checkbox"]', function () {
            $(".menuGroup").css("display", "none");
            var id = $(this).attr('data-id');
            var pos = listRuleSelected.indexOf(id);
            if (pos == -1) {
                listRuleSelected.push(id);
            }
            else {
                listRuleSelected.splice(pos, 1);
            }
        });
        $("#resultsGroup").on("click", '.btnMenu', function () {
            var id = $(this).attr('data-id');
            idGroupEdit = id;
            $(".menuGroup").css("display", "none");
            var item = $('[menu-id=' + id + ']');
            item.css('display', 'block');
        });
        $("#resultsGroup").on("click", ".btnCancel", function () {
            $(".menuGroup").css("display", "none");
        });
        $("#btnSearch").click(function () {
            $(".menuGroup").css("display", "none");
            data.group_id = $("#group_id").val();
            data.group_name = $("#group_name").val();
            data.group_comment = $("#group_comment").val();
            data.page = 1;
            data.action = "search";
            loadGroup(data, listGroupSelected);
        });
        $("#btnCancel").click(function () {
            action = "load";
            data = {
                action: action,
                pageSize: 50,
                page: 1,
                group_id: "",
                group_name: "",
                group_comment: ""
            };
            $(".menuGroup").css("display", "none");
            $("#group_id").val("");
            $("#group_name").val("");
            $("#group_comment").val("");
            listGroupSelected = [];
            $("#selectPageSize").val(50).change();
        });
        $("#selectPageSize").change(function () {
            data.pageSize = $(this).val();
            loadGroup(data, listGroupSelected);
        });
        $("#btnModalChangeRule").click(function () {
            $(".menuUser").css("display", "none");
            $('.listGroupSelected').html("");
            $('.listRule').html("");
            for (var i = 0; i < listRule.length; i++) {
                var html = '<div class="itemRule">' +
                        '<input class="ckbRule" type="checkbox" data-id="' + listRule[i].rule_id + '"/>' +
                        '<label>' + listRule[i].rule_name + '</label>' +
                        '</div>';
                $(".listRule").append(html);
            }
            for (i = 0; i < listGroupSelected.length; i++) {
                var rs = '<label class="labelGroupSelected">';
                rs += listGroupSelected[i];
                rs += '</label>';
                $('.listGroupSelected').append(rs);
            }
            $('#modalChangeGroup').modal('toggle');
        });
        $("#btnChangeRule").click(function () {
            if (listGroupSelected.length == 0) {
                return;
            }
            changeRule(listGroupSelected, listRuleSelected);
            listGroupSelected = [];
            listRuleSelected = [];
            loadGroup(data, listGroupSelected);
        });
        $('#modalGroup').on('hidden.bs.modal', function () {
            listRuleSelected = [];
            if (optionUpdateGroup = "insert") {
                loadGroup(data, listGroupSelected);
            }
            refreshModalGroup();
        })
        $('#modalChangeRule').on('hidden.bs.modal', function () {
            listRuleSelected = [];
        })
        $("#btnAddGroup").click(function () {
            $(".menuGroup").css("display", "none");
            refreshModalGroup();
            $("#titleModalGroup").html("Add Group");
            optionUpdateGroup = "insert";
            $('.listRule').html("");
            for (var i = 0; i < listRule.length; i++) {
                var html = "";

                html = '<div class="itemRule">' +
                '<input class="ckbRule" type="checkbox" data-id="' + listRule[i].rule_id + '"/>' +
                '<label>' + listRule[i].rule_name + '</label>' +
                '</div>';
                $(".listRule").append(html);
            }
            $('#modalGroup').modal('toggle');
        });
        $("#resultsGroup").on("click", ".btnEdit", function () {
            var group = getGroupById(idGroupEdit);
            getGroupToEdit(group);
            $("#titleModalGroup").html("Edit Group");
            $(".menuGroup").css("display", "none");
            optionUpdateGroup = "update";
            var groupRules = getRulesByGroupId(idGroupEdit);
            $('.listRule').html("");
            for (var i = 0; i < listRule.length; i++) {
                var html = "";
                html = '<div class="itemRule">' +
                '<input class="ckbRule" type="checkbox" data-id="' + listRule[i].rule_id + '"/>' +
                '<label>' + listRule[i].rule_name + '</label>' +
                '</div>';
                for (var j = 0; j < groupRules.length; j++) {
                    if (listRule[i].rule_id == groupRules[j].rule_id) {
                        html = '<div class="itemRule">' +
                        '<input class="ckbRule" type="checkbox" checked = "true" data-id="' + listRule[i].rule_id + '"/>' +
                        '<label>' + listRule[i].rule_name + '</label>' +
                        '</div>';
                        listRuleSelected.push(listRule[i].rule_id.toString());
                    }
                }
                $(".listRule").append(html);
            }
            $('#modalGroup').modal('toggle');
        });
        $("#resultsGroup").on("click", ".btbDelete", function () {
            $('.listGroupSelected').html("");
            var rs = '<label class="labelGroupSelected">';
            rs += idGroupEdit;
            rs += '</label>';
            $('.listGroupSelected').append(rs);
            $(".menuGroup").css("display", "none");
            optionUpdateGroup = "delete";
            $('#modalDeleteGroup').modal('toggle');
        });
        $("#btnDeleteGroup").click(function () {
            var group = {
                group_id: idGroupEdit,
                optionUpdateGroup: optionUpdateGroup
            };
            optionUpdateGroup = "delete";
            updateGroup(group, optionUpdateGroup);
            loadGroup(data, listGroupSelected);
        });
        $("#btnSaveGroup").click(function () {
            if (optionUpdateGroup == "insert") {
                var group = {
                    group_id: null,
                    group_name: $('#input_group_name').val(),
                    group_comment: $('#input_group_comment').val(),
                    rules: listRuleSelected,
                    optionUpdateGroup: optionUpdateGroup
                };
                updateGroup(group, optionUpdateGroup);
                $('#modalGroup').modal('toggle');
                loadGroup(data, listGroupSelected);
            }
            if (optionUpdateGroup == "update") {
                var group = {
                    group_id: idGroupEdit,
                    group_name: $('#input_group_name').val(),
                    group_comment: $('#input_group_comment').val(),
                    rules: listRuleSelected,
                    optionUpdateGroup: optionUpdateGroup
                };
                updateGroup(group, optionUpdateGroup);
                $('#modalGroup').modal('toggle');
                loadGroup(data, listGroupSelected);
            }
        });
        $('#input_group_name').change(function () {
            var group_name = $('#input_group_name').val();
            var group = getGroupByName(group_name);
            //if name group exist
            if (group != null) {
                $("#lb_group_name").html("Group name is exist");
                $("#lb_group_name").css("color", "red");
                $("#input_group_name").css("border-color", "red");
                $("#btnSaveGroup").prop('disabled', true);
            }
            else {
                $("#lb_group_name").html("Group name");
                $("#lb_group_name").css("color", "");
                $("#input_group_name").css("border-color", "");
                $("#btnSaveGroup").prop('disabled', false);
            }
        });
    });
    function loadGroup(data, listGroupSelected) {
        //Show image loading when load data
        if ($(".imgLoading").css("display") == "none") {
            $(".imgLoading").show();
        }
        var urlLoad = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "group-load") {
                urlLoad = groupRule[i].url;
            }
        }
        if (urlLoad == "") {
            var rs = '<div align="center">Empty Data</div>';
            $("#notice").html(rs);
            $("#lbFrom").text(0);
            $("#lbTo").text(0);
            $("#lbTotal").text(0);
            //Hide image loading
            $(".imgLoading").hide();
            var url = 'group-user?page=' + urlPage;
            window.history.pushState({path: url}, '', url);
            return;
        }
        urlLoad = '{{url('/')}}' + urlLoad;
        var urlPage = data.page;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlLoad,
            method: "post",
            data: {
                data: data,
                _token: _token
            },
            success: function (data) {
                $(".myPagination").html(data.paginate);
                $("#resultsGroup").html("");
                $("#notice").html("");
                var listGroup = data.data.data;
                var countTotal = data.data.total;
                var countFrom = data.data.from;
                var countTo = data.data.to;
                var disabled = "disabled";
                var i = 0;
                for (i = 0; i < listGroup.length; i++) {
                    var rs = '<tr>';
                    rs += '<td><button class="btnMenu" data-id="' + listGroup[i].group_id + '"><img src="{{ asset('/img/icon_edit.svg') }}"></button><label class="lb_group_id">' + listGroup[i].group_id + '</label>' +
                    '<div class="menuGroup" menu-id="' + listGroup[i].group_id + '">' +
                    '<div class="menuGroup-content">' +
                    '<div class="itemMenu"><a class="btnEdit" href="#">Edit</a></div>' +
                    '<div class="itemMenu"><a class="btbDelete" href="#">Delete</a></div>' +
                    '<div class="itemMenu"><a class="btnCancel" href="#">Cancel</a></div>' +
                    '</div>' +
                    '</div>' +
                    '</td>';
                    var color = ""
                    if (listGroup[i].group_id == 1) {
                        color = "label label-danger";
                    }
                    else if (listGroup[i].group_id == 2) {
                        color = "label label-primary";
                    }
                    else if (listGroup[i].group_id == 3) {
                        color = "label label-success";
                    }
                    else {
                        color = "label label-default";
                    }
                    rs += '<td align="center"><label class="' + color + '">' + listGroup[i].group_name + '</label></td>';
                    rs += '<td align="center">' + listGroup[i].group_comment + '</td>';
                    var pos = listGroupSelected.indexOf(listGroup[i].group_id.toString());
                    if (pos == -1) {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" data-id="' + listGroup[i].group_id + '"/></td>';
                    }
                    else {
                        rs += '<td align="center"><input class="ckbSelect" type="checkbox" checked = "true" data-id="' + listGroup[i].group_id + '"/></td>';
                    }
                    rs += '</tr>';
                    $("#resultsGroup").append(rs);
                }
                if (listGroup.length == 0) {
                    var rs = '<div align="center">Empty Data</div>';
                    $("#notice").html(rs);
                    countFrom = 0;
                    countTo = 0;
                }
                $("#lbFrom").text(countFrom);
                $("#lbTo").text(countTo);
                $("#lbTotal").text(countTotal);
                //Hide image loading
                $(".imgLoading").hide();
                var url = 'group-user?page=' + urlPage;
                window.history.pushState({path: url}, '', url);
            }
        });
    }
    function changeRule(listGroupSelected, listRuleSelected) {
        var urlChangeRule = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "group-change-rule") {
                urlChangeRule = groupRule[i].url;
            }
        }
        if (urlChangeRule == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlChangeRule = '{{url('/')}}' + urlChangeRule;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlChangeRule,
            method: "post",
            async: false,
            data: {
                listGroupSelected: listGroupSelected,
                listRuleSelected: listRuleSelected,
                _token: _token
            },
            success: function () {
            }
        });
    }
    function updateGroup(group) {
        var urlUpdateGroup = "";
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "group-update") {
                urlUpdateGroup = groupRule[i].url;
            }
        }
        if (urlUpdateGroup == "") {
            $('#modalPermission').modal('toggle');
            return;
        }
        urlUpdateGroup = '{{url('/')}}' + urlUpdateGroup;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: urlUpdateGroup,
            method: "post",
            async: false,
            data: {
                group: group,
                _token: _token
            },
            success: function () {
            }
        });
    }
    function refreshModalGroup() {
        $("#lb_group_name").html("Group name");
        $("#lb_group_name").css("color", "");
        $("#input_group_name").css("border-color", "");
        $("#lb_group_comment").html("Group comment");
        $("#lb_group_comment").css("color", "");
        $("#input_group_comment").css("border-color", "");
        $("#btnSaveGroup").prop('disabled', false);

        $("#input_group_name").val("");
        $("#input_group_comment").val("");
    }
    function getGroupToEdit(group) {
        $("#input_group_name").val(group.group_name);
        $("#input_group_comment").val(group.group_comment);
    }
    function getGroupUserLogin() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{url('/user/get-group')}}',
            method: "post",
            async: false,
            success: function (data) {
                groupUser = data.groupUser;
                groupRule = data.groupRule;
            }
        });
    }
    function getAllRule() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '{{route('get-all-rule-user')}}',
            method: "post",
            async: false,
            success: function (data) {
                var listRuleUser = data.listRuleUser;
                for (var i = 0; i < listRuleUser.length; i++) {
                    listRule.push(listRuleUser[i]);
                }
            }
        });
    }
    function getGroupByName(group_name) {
        var group = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('get-group-by-name') }}",
            method: "post",
            async: false,
            data: {
                group_name: group_name,
                _token: _token
            },
            success: function (data) {
                group = data.group[0];
            }
        });
        return group;
    }
    function getGroupById(group_id) {
        var group = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('get-group-by-id') }}",
            method: "post",
            async: false,
            data: {
                group_id: group_id,
                _token: _token
            },
            success: function (data) {
                group = data.group[0];
            }
        });
        return group;
    }
    function getRulesByGroupId(group_id) {
        var rule = null;
        var _token = $('input[name="_token"]').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('get-rules-by-group-id') }}",
            method: "post",
            async: false,
            data: {
                group_id: group_id,
                _token: _token
            },
            success: function (data) {
                rule = data.rules;
            }
        });
        return rule;
    }
    function checkAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = true;
            var id = listCheckBox[i].getAttribute("data-id").toString();
            var pos = listGroupSelected.indexOf(id);
            if (pos == -1) {
                listGroupSelected.push(id);
            }
        }
    }
    function UnCheckAll() {
        var listCheckBox = document.getElementsByClassName("ckbSelect");
        for (var i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = false;
            var id = listCheckBox[i].getAttribute("data-id").toString();
            var pos = listGroupSelected.indexOf(id);
            if (pos != -1) {
                listGroupSelected.splice(pos, 1);
            }
        }
    }
    function checkPermission() {
        var flag = false;
        for (var i = 0; i < groupRule.length; i++) {
            if (groupRule[i].rule_name == "group-load") {
                flag = true;
            }
        }
        if (flag == false) {
            $('#modalPermission').modal('toggle');
            var url = '{{url('/')}}';
            $(location).attr('href', url);
            return;
        }
    }
</script>