<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class VOrder extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'v_order';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'mysql_edi';
    /**
     * Post per page.
     * @var int
     */

    protected $paginator;
    protected $collection;
    protected $totalItems  = 0;
    protected $routing     = 'admin';
    protected $currentPage = 1;
    /**
     * Post per page.
     * @var int
     */
    protected $page = 20;

    /**
     * Get total count order by suppliercd.
     * @param string $suppliercd supplier code
     * @return int
     */
    public function getCountOrder($suppliercd)
    {
        /*$typeOrder = Config::get('edi.m_order_type_id_order');*/
        $data = $this->where('suppliercd', "!=", $suppliercd)
            ->where('m_order_type_id', "=", 1);
        $count = $data->count();
        return $count;
    }
    /**
     * Get list order data
     *
     * @param  array  $arrayQuery   Array condition get data
     * @param  array  $arrSort      Sort type
     * @param  array  $arraySearch  Array condition search
     * @param  bool   $isCsv
     * @return object
     */
    public function getListOrder($arrayQuery, $arrSort = null, $arraySearch = null, $isCsv = false)
    {
        $data = $this->where('suppliercd', '!=', $arrayQuery['suppliercd'])
            ->where('state_flg', '=', $arrayQuery['state_flg']);

        if (!empty($arraySearch['order_date_from'])) {
            $data->where('order_date', '>=', $arraySearch['order_date_from']);
        }
        if (!empty($arraySearch['order_date_to'])) {
            $data->where('order_date', '<=', $arraySearch['order_date_to']);
        }
        if (!empty($arraySearch['supplykb1'])) {
            $data->where('supplykb', '=', '客注');
        }
        if (!empty($arraySearch['supplykb2'])) {
            $data->where('supplykb', '=', '月初');
        }
        if (!empty($arraySearch['name'])) {
            $arrName = $this->strExplode($arraySearch['name']);
            $data->where(function ($query) use ($arrName) {
                foreach ($arrName as $name) {
                    $query->orWhere('name', 'LIKE', '%' . $name . '%');
                }
            });
        }
        if (!empty($arraySearch['supplieritemcd'])) {
            $data->where('supplieritemcd', '=', $arraySearch['supplieritemcd']);
        }
        if (!empty($arraySearch['jan_code'])) {
            $arrJanCode = $this->strExplode($arraySearch['jan_code']);
            $data->where(function ($query) use ($arrJanCode) {
                foreach ($arrJanCode as $janCode) {
                    $query->orWhere('jan_code', 'LIKE', '%' . $janCode . '%');
                }
            });
        }
        if (!empty($arraySearch['maker_code'])) {
            $arrMakerCode = $this->strExplode($arraySearch['maker_code']);
            $data->where(function ($query) use ($arrMakerCode) {
                foreach ($arrMakerCode as $makerCode) {
                    $query->orWhere('maker_code', 'LIKE', '%' . $makerCode . '%');
                }
            });
        }
        if (!empty($arraySearch['order_code'])) {
            $arrOrderCode = $this->strExplode($arraySearch['order_code']);
            $data->where(function ($query) use ($arrOrderCode) {
                foreach ($arrOrderCode as $orderCode) {
                    $query->orWhere('order_code', '=', $orderCode);
                }
            });
        }
        if (!empty($arraySearch['memo'])) {
            $arrMemo = $this->strExplode($arraySearch['memo']);
            $data->where(function ($query) use ($arrMemo) {
                foreach ($arrMemo as $memo) {
                    $query->orWhere('memo', 'LIKE', '%' . $memo . '%');
                }
            });
        }
        if (!empty($arraySearch['send_fax_date'])) {
            $sendFaxDate = $arraySearch['send_fax_date'];
            $data->where(function ($query) use ($sendFaxDate) {
                $query->where('send_fax_date', '=', $sendFaxDate)
                    ->orWhere('send_cancel_fax_date', '=', $sendFaxDate);
            });
        }
        if (!empty($arraySearch['reminder_flg'])) {
            $data->whereIn('reminder_flg', [$arraySearch['reminder_flg']]);
        }
        if (!empty($arraySearch['m_order_type_id'])) {
            $data->whereIn('v_order.m_order_type_id', [$arraySearch['m_order_type_id']]);
        }
        if (!empty($arraySearch['percent'])) {
            $data->whereRaw('(t_o_price*(5/100)+t_o_price) < price');
        }
        if ($arrSort !== null) {
            foreach ($arrSort as $field => $type) {
                $data->orderBy($field, $type);
            }
        }

        $data->select('v_order.t_order_id', 'v_order.order_date', 'v_order.order_code', 'v_order.item_code')
            ->addSelect('v_order.jan_code', 'v_order.maker_code', 'v_order.name', 'v_order.delivery')
            ->addSelect('v_order.color', 'v_order.size', 'v_order.t_o_cancel_flg', 'v_order.t_o_price')
            ->addSelect('v_order.supplieritemcd', 'v_order.supplykb', 'v_order.t_order_detail_id')
            ->addSelect('v_order.shipment_date_plan', 'v_order.quantity', 'v_order.price', 'v_order.other')
            ->addSelect('v_order.direct_flg', 'v_order.pass', 'v_order.reminder_flg', 'v_order.memo')
            ->addSelect('v_order.t_o_d_cancel_flg', 'v_order.m_order_type_id', 'v_order.t_o_quantity');
        if ($isCsv) {
            $data->where(function ($subWhere) {
                $subWhere->where('t_o_cancel_flg', 0)
                    ->where('t_o_d_cancel_flg', 0);
            });
            return $data->get();
        }
        $this->page = $arrayQuery['per_page'];
        return $data->paginate($this->page);
    }
    /**
     * Explode string to array by space
     *
     * @param   string   $str   String source
     * @return  array
     */
    public static function strExplode($str)
    {
        $data = mb_convert_kana($str, 's');
        return preg_split('/[\s]+/', $data, -1, PREG_SPLIT_NO_EMPTY);
    }
}
