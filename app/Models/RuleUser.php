<?php
/**
 * @package App\Models
 * @subpackage Model
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Exe sql for rule_mst_user table
 *
 * Class GroupUser
 * @package App\Models
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class RuleUser extends Model
{
    /**
     * The database table used by the model.
     * @var    string
     */
    protected $table = 'rule_mst_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'rule_id';

    /**
     * Turn on/off created_at and updated_at field
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get address route by field rule_id
     * @param $ruleId
     * @return array
     */
    public function getRouteByRuleId($ruleId)
    {
        $data = $this->find($ruleId)->toArray();
        return $data;
    }

    /**
     * Get all data of table rule_mst_user
     * @return array
     */
    public function getAllRule()
    {
        return $this->get();
    }
}
