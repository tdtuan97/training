<?php
/**
 * @package App\Models
 * @subpackage Model
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Exe sql for mst_user table
 *
 * Class MstUser
 * @package App\Models
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class MstUser extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     * @var    string
     */
    protected $table = 'mst_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 't_admin_id';

    /**
     * Turn on/off created_at and updated_at field
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        't_admin_id',
        'email',
        'password',
        'is_active',
        'group_id',
        'last_login_at',
        'last_login_ip',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get a listing record per page of table
     * @param $params
     * @return array
     */
    public function getListMstUser($params)
    {
        return $listMstUser = $this->leftjoin('group_user', 'mst_user.group_id', '=', 'group_user.group_id')
            ->paginate($params['pageSize'], ['*'], 'page', $params['currentPage']);
    }

    /**
     * Get a listing record per page by request search of table
     * @param $params
     * @return array
     */
    public function getListMstUserByRequest($params)
    {
        $txtAdminId = $params['t_admin_id'];
        $keywordsAdminId = explode(" ", $txtAdminId);
        $txtEmail = $params['email'];
        $keywordsEmail = explode(" ", $txtEmail);
        $results = $this->Where(function ($results) use ($keywordsAdminId) {
            foreach ($keywordsAdminId as $keywordAdminId) {
                if ($keywordAdminId != "") {
                    $results->orWhere('t_admin_id', 'LIKE', '%' . $keywordAdminId . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsEmail) {
            foreach ($keywordsEmail as $keywordEmail) {
                if ($keywordEmail != "") {
                    $results->orWhere('email', 'LIKE', '%' . $keywordEmail . '%');
                }
            }
        });
        if ($params['activate'] == "true" && $params['deactivate'] == "true") {
            $results = $results;
        }
        if ($params['activate'] == "true" && $params['deactivate'] == "false") {
            $results = $results->where('is_active', '=', 1);
        }
        if ($params['activate'] == "false" && $params['deactivate'] == "true") {
            $results = $results->where('is_active', '=', 0);
        }
        if ($params['activate'] == "false" && $params['deactivate'] == "false") {
            $results = $results->where('is_active', '=', -1);
        }
        return $results->leftjoin('group_user', 'mst_user.group_id', '=',
            'group_user.group_id')->paginate($params['pageSize'], ['*'], 'page', $params['currentPage']);
    }

    /**
     * Get record by field email
     * @param $email
     * @return mixed
     */
    public function getMstUserByEmail($email)
    {
        return $data = $this->where('email', '=', $email)->get();
    }

    /**
     * Get record by field t_admin_id
     * @param $t_admin_id
     * @return mixed
     */
    public function getMstUserById($t_admin_id)
    {
        return $data = $this->where('t_admin_id', '=', $t_admin_id)->get();
    }

    /**
     * Get group of user by field t_admin_id
     * @param $adminId
     * @return GroupUser
     */
    public function getGroupUserByUserId($adminId)
    {
        $user = $this->find($adminId)->toArray();
        $objGroupUser = new GroupUser();
        return $objGroupUser->getGroupByGroupID($user['group_id']);
    }

    /**
     * Get list rule of group by field group_id
     * @param $groupId
     * @return array
     */
    public function getRulesByGroupId($groupId)
    {
        $objRule = new RuleUser();
        $objGroupRule = new GroupRule();
        $groupRuleID = $objGroupRule->getRulesIdByGroupId($groupId);
        $groupUrl = array();
        for ($i = 0; $i < count($groupRuleID); $i++) {
            $ruleId = $groupRuleID[$i]['rule_id'];
            $route = $objRule->getRouteByRuleId($ruleId);
            array_push($groupUrl, $route);
        }
        return $groupUrl;
    }

    /**
     * Change value field is_active of user
     * @param $params
     */
    public function changeStatusUser($params)
    {
        $listUserSelected = $params['listUserSelected'];
        $option = $params['option'];
        if ($option == "active") {
            foreach ($listUserSelected as $user) {
                $this->where('t_admin_id', $user)->update(['is_active' => 1]);
            }
        }
        if ($option == "deactive") {
            foreach ($listUserSelected as $user) {
                $this->where('t_admin_id', $user)->update(['is_active' => 0]);
            }
        }
    }

    /**
     * Change value field group_id of user
     * @param $params
     */
    public function changeGroupUser($params)
    {
        $listUserSelected = $params['listUserSelected'];
        $groupId = $params['groupId'];
        foreach ($listUserSelected as $user) {
            $this->where('t_admin_id', $user)->update(['group_id' => $groupId]);
        }
    }

    /**
     * Insert record into table
     * @param $params
     */
    public function insertMstUser($params)
    {
        $user = array(
            'email' => $params['email'],
            'password' => bcrypt($params['password']),
        );
        $this->insert($user);
    }

    /**
     * update record into table
     * @param $params
     */
    public function updateMstUser($params)
    {
        $user = $this->find($params['t_admin_id']);
        if ($params['flagChangePassword'] == "true") {
            $user->password = bcrypt($params['password']);
        }
        $user->email = $params['email'];
        $user->save();
    }

    /**
     * delete record from table
     * @param $params
     */
    public function deleteMstUser($params)
    {
        $user = $this->find($params['t_admin_id']);
        $user->delete();
    }
}
