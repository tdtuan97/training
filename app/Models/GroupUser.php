<?php
/**
 * @package App\Models
 * @subpackage Model
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Exe sql for group_user table
 *
 * Class GroupUser
 * @package App\Models
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class GroupUser extends Model
{
    /**
     * The database table used by the model.
     * @var    string
     */
    protected $table = 'group_user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'group_id';

    /**
     * Turn on/off created_at and updated_at field
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id',
        'group_name',
        'group_comment'
    ];

    /**
     * Get a listing record per page of table
     * @param $params
     * @return array
     */
    public function getListGroupUser($params)
    {
        return $listGroupUser = $this->paginate($params['pageSize'], ['*'], 'page', $params['currentPage']);
    }

    /**
     * Get a listing record per page by request search of table
     * @param $params
     * @return array
     */
    public function getListGroupUserByRequest($params)
    {
        $txtGroupId = $params['group_id'];
        $keywordsGroupId = explode(" ", $txtGroupId);
        $txtGroupName = $params['group_name'];
        $keywordsGroupName = explode(" ", $txtGroupName);
        $txtGroupComment = $params['group_comment'];
        $keywordsGroupComment = explode(" ", $txtGroupComment);
        $results = $this->Where(function ($results) use ($keywordsGroupId) {
            foreach ($keywordsGroupId as $keywordGroupId) {
                if ($keywordGroupId != "") {
                    $results->orWhere('group_id', 'LIKE', '%' . $keywordGroupId . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsGroupName) {
            foreach ($keywordsGroupName as $keywordGroupName) {
                if ($keywordGroupName != "") {
                    $results->orWhere('group_name', 'LIKE', '%' . $keywordGroupName . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsGroupComment) {
            foreach ($keywordsGroupComment as $keywordGroupComment) {
                if ($keywordGroupComment != "") {
                    $results->orWhere('group_comment', 'LIKE', '%' . $keywordGroupComment . '%');
                }
            }
        });
        return $results->paginate($params['pageSize'], ['*'], 'page', $params['currentPage']);
    }

    /**
     * Insert record into table
     * @param $params
     */
    public function insertGroupUser($params)
    {
        $objGroupRule = new GroupRule();
        $rules = $params['rules'];
        $group = array(
            'group_name' => $params['group_name'],
            'group_comment' => $params['group_comment']
        );
        $this->insert($group);
        $group = $this->getGroupUserByName($group['group_name']);
        foreach ($rules as $rule) {
            $objGroupRule->insertGroupRule($group[0]['group_id'], $rule);
        }
    }

    /**
     * update record into table
     * @param $params
     */
    public function updateGroupUser($params)
    {
        $objGroupRule = new GroupRule();
        $rules = $params['rules'];
        $group = $this->find($params['group_id']);
        $group->group_name = $params['group_name'];
        $group->group_comment = $params['group_comment'];
        $group->save();
        $objGroupRule->deleteGroupRuleByGroupId($params['group_id']);
        foreach ($rules as $rule) {
            $objGroupRule->insertGroupRule($params['group_id'], $rule);
        }
    }

    /**
     * delete record from table
     * @param $params
     */
    public function deleteGroupUser($params)
    {
        $objGroupRule = new GroupRule();
        $objGroupRule->deleteGroupRuleByGroupId($params['group_id']);
        $group = $this->find($params['group_id']);
        $group->delete();
    }

    /**
     * Change fill group_id rule_id of record in table group_rules
     * @param $listGroupSelected
     * @param $listRuleSelected
     * @internal param $params
     */
    public function changeRule($listGroupSelected, $listRuleSelected)
    {

        foreach ($listGroupSelected as $groupSelected) {
            $objGroupRule = new GroupRule();
            $objGroupRule->deleteGroupRuleByGroupId($groupSelected);
            foreach ($listRuleSelected as $rule) {
                $objGroupRule->insertGroupRule($groupSelected, $rule);
            }
        }
    }

    /**
     * Get list record of table group_user
     * @return array
     */
    public function getAllDataGroupUser()
    {
        return $this->get();
    }

    /**
     * Get record by field group_id
     * @param $groupId
     * @return GroupUser
     */
    public function getGroupByGroupID($groupId)
    {
        return $this->find($groupId)->toArray();
    }

    /**
     * Get record by field group_name
     * @param $group_id
     * @return array
     */
    public function getGroupUserById($group_id)
    {
        return $data = $this->where('group_id', '=', $group_id)->get();
    }

    /**
     * Get record by field group_name
     * @param $group_name
     * @return array
     */
    public function getGroupUserByName($group_name)
    {
        return $data = $this->where('group_name', '=', $group_name)->get();
    }
}
