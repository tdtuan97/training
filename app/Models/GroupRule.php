<?php
/**
 * @package App\Models
 * @subpackage Model
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Exe sql for group_rule table
 *
 * Class GroupRule
 * @package App\Models
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class GroupRule extends Model
{
    /**
     * The database table used by the model.
     * @var    string
     */
    protected $table = 'group_rule';

    /**
     * Turn on/off created_at and updated_at field
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get list rule id by group id
     * @param $groupId
     * @return array
     */
    public function getRulesIdByGroupId($groupId)
    {
        return $this->select('rule_id')->where('group_id', '=', $groupId)->get();
    }

    /**
     * Get list rule by group id
     * @param $groupId
     * @return array
     */
    public function getRulesByGroupId($groupId)
    {
        return $rules = $this->where('group_id', '=', $groupId)
            ->leftjoin('rule_mst_user', 'group_rule.rule_id', '=', 'rule_mst_user.rule_id')
            ->get();
    }

    /**
     * Insert record into table
     * @param $groupId
     * @param $ruleId
     */
    public function insertGroupRule($groupId, $ruleId)
    {
        $data = array(
            'group_id' => $groupId,
            'rule_id' => $ruleId
        );
        $this->insert($data);
    }

    /**
     * Delete record from table
     * @param $groupId
     */
    public function deleteGroupRuleByGroupId($groupId)
    {
        $this->where('group_id', '=', $groupId)->delete();
    }
}
