<?php
/**
 * Exe sql for edi.m_order_type
 *
 * @package App\Models\Edi
 * @subpackage EdiModel
 * @copyright Copyright (c) 2017 CriverCrane! Corporation. All Rights Reserved.
 * @author lam.vinh<lam.vinh@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TMorderType extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'm_order_type';
    /**
     * The database is used by the model.
     * @var string
     */
    protected $connection = 'mysql_edi';

    /**
     * Get total type order of screen wait.
     * @return object
     */
    public function getTypeOrderWait()
    {
        $data = $this->where("wait_search_flg", "=", 1)
            ->where("del_flg", "=", 0)
            ->where("valid_flg", "=", 1)
            ->orderBy('wait_search_by', 'asc')
            ->get(['m_order_type_id', 'name']);
        return $data;
    }
    /**
     * Get total type order of screen all.
     * @return object
     */
    public function getTypeOrderAll()
    {
        $data = $this->where("all_search_flg", "=", 1)
            ->where("del_flg", "=", 0)
            ->where("valid_flg", "=", 1)
            ->orderBy('all_search_by', 'asc')
            ->get(['m_order_type_id', 'name']);
        return $data;
    }

    /**
     * Get order type of screen order
     *
     * @return object
     */
    public function getOrderType()
    {
        return $this->where('order_search_flg', '=', 1)
            ->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->orderBy('order_search_by', 'asc')
            ->get(['m_order_type_id', 'name']);
    }

    /**
     * Get order type radio of screen order
     *
     * @return object
     */
    public function getOrderTypeRadio()
    {
        return $this->where('order_option_flg', '=', 1)
            ->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->orderBy('order_option_by', 'asc')
            ->get(['m_order_type_id', 'name']);
    }
    /**
     * Get order type radio of direct list all
     *
     * @return object
     */
    public function getAllOrderType()
    {
        return $this->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->orderBy('order_option_by', 'asc')
            ->get(['m_order_type_id', 'name']);
    }

    /**
     * Check Order type by name
     *
     * @param   string  $name
     * @param   boolean $select
     * @return  mixed
     */
    public static function checkOrderTypeByName($name, $select = false)
    {
        $data = self::where('name', '=', $name)
            ->where('order_option_flg', '=', 1)
            ->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->first();
        if ($data === null) {
            return false;
        }
        if ($select) {
            return $data;
        }
        return true;
    }

    /**
     * Check Order type by name
     *
     * @param   string  $name
     * @param   boolean $select
     * @return  mixed
     */
    public static function checkZaikoTypeByName($name, $select = false)
    {
        $data = self::where('name', '=', $name)
            ->where('zaiko_option_flg', '=', 1)
            ->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->first();
        if ($data === null) {
            return false;
        }
        if ($select) {
            return $data;
        }
        return true;
    }

    /**
     * Get zaiko type
     *
     * @return object
     */
    public function getZaikoType()
    {
        $data = $this->where('zaiko_option_flg', '=', 1)
            ->where('valid_flg', '=', 1)
            ->where('del_flg', '=', 0)
            ->get();
        return $data;
    }
}
