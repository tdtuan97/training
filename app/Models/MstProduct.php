<?php
/**
 * @package App\Models
 * @subpackage Model
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Exe sql for mst_product table
 *
 * Class MstProduct
 * @package App\Models
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class MstProduct extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';
    /**
     * The database table used by the model.
     * @var    string
     */
    protected $table = 'mst_product';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'product_code';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
    /**
     * Turn on/off created_at and updated_at field
     *
     * @var boolean
     */
    public $timestamps = true;
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'in_date';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'up_date';

    /**
     * Get list product of table by price supplier id
     *
     * @param $params
     * @param $adminId
     * @return array
     */
    public function getListMstProduct($params, $adminId)
    {
        return $listProduct = $this
            ->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name', 'list_price',
                'process_status')
            ->where('price_supplier_id', '=', $adminId)->paginate($params['pageSize'], ['*'], 'page',
                $params['currentPage']);
    }

    /**
     * Get list product of table by price supplier id and request
     *
     * @param $params
     * @param $adminId
     * @return array
     */
    public function getListMstProductByRequest($params, $adminId)
    {
        $results = $this->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name',
            'list_price', 'process_status')
            ->where('price_supplier_id', '=', $adminId);
        $txtProductJan = $params['product_jan'];
        $keywordsProductJan = explode(" ", $txtProductJan);
        $txtMakerFullNm = $params['maker_full_nm'];
        $keywordsMakerFullNm = explode(" ", $txtMakerFullNm);
        $txtProductName = $params['product_name'];
        $keywordsProductName = explode(" ", $txtProductName);
        $txtProductCode = $params['product_code'];
        $keywordsProductCode = explode(" ", $txtProductCode);
        $results->Where(function ($results) use ($keywordsProductJan) {
            foreach ($keywordsProductJan as $keywordProductJan) {
                if ($keywordProductJan != "") {
                    $results->orWhere('product_jan', 'LIKE', '%' . $keywordProductJan . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsMakerFullNm) {
            foreach ($keywordsMakerFullNm as $keywordMakerFullNm) {
                if ($keywordMakerFullNm != "") {
                    $results->orWhere('maker_full_nm', 'LIKE', '%' . $keywordMakerFullNm . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsProductName) {
            foreach ($keywordsProductName as $keywordProductName) {
                if ($keywordProductName != "") {
                    $results->orWhere('product_name', 'LIKE', '%' . $keywordProductName . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsProductCode) {
            foreach ($keywordsProductCode as $keywordProductCode) {
                if ($keywordProductCode != "") {
                    $results->orWhere('product_code', 'LIKE', '%' . $keywordProductCode . '%');
                }
            }
        });
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "true") {
            $results = $results;
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '<', 2);
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '!=', 1);
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '=', 0);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '>', 0);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '=', 1);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '=', 2);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '>', 2);
        }
        return $results->paginate($params['pageSize'], ['*'], 'page', $params['currentPage']);
    }

    /**
     *  Get a product by product code and price supplier id
     * @param $params
     * @param $adminId
     * @return array
     */
    public function getMstProductByProductCode($params, $adminId)
    {
        return $data = $this->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name',
            'list_price', 'process_status')
            ->where('price_supplier_id', '=', $adminId)->where('product_code', '=', $params)->get();
    }

    /**
     * Insert record into table
     * @param $params
     * @param $adminId
     */
    public function insertMstProduct($params, $adminId)
    {
        $product = array(
            'cheetah_status' => $params['cheetah_status'],
            'maker_full_nm' => $params['maker_full_nm'],
            'product_code' => $params['product_code'],
            'product_jan' => $params['product_jan'],
            'product_name' => $params['product_name'],
            'list_price' => $params['list_price'],
            'process_status' => $params['process_status'],
            'price_supplier_id' => $adminId
        );
        $this->insert($product);
    }

    /**
     * update record into table
     * @param $params
     * @param $adminId
     */
    public function updateMstProduct($params, $adminId)
    {
        $product = $this->find($params['product_code']);
        $product->cheetah_status = $params['cheetah_status'];
        $product->maker_full_nm = $params['maker_full_nm'];
        $product->product_jan = $params['product_jan'];
        $product->product_name = $params['product_name'];
        $product->list_price = $params['list_price'];
        $product->process_status = $params['process_status'];
        $product->price_supplier_id = $adminId;
        $product->save();
    }

    /**
     * delete record from table
     * @param $params
     * @param $adminId
     */
    public function deleteMstProduct($params, $adminId)
    {
        $product = $this->find($params['product_code']);
        $product->delete();
    }

    /**
     * Change status product mst_product of table
     *
     * @param $params
     */
    public function changeStatusProduct($params)
    {
        $listProductSelected = $params['listProductSelected'];
        if ($params['option'] == 1) {
            foreach ($listProductSelected as $product) {
                $this->where('product_code', $product)->update(['cheetah_status' => 2, 'process_status' => 1]);
            }
        }
        if ($params['option'] == 2) {
            foreach ($listProductSelected as $product) {
                $this->where('product_code', $product)->update(['cheetah_status' => 1, 'process_status' => 1]);
            }
        }
        if ($params['option'] == 3) {
            foreach ($listProductSelected as $product) {
                $this->where('product_code', $product)->update(['cheetah_status' => 0, 'process_status' => 1]);
            }
        }
    }

    /**
     * Get product to export file .csv
     *
     * @param $params
     * @param $adminId
     * @param $fileName
     * @return array
     */
    public function getProductExportToCSV($params, $adminId, $fileName)
    {
        $results = $this->select('cheetah_status', 'maker_full_nm', 'product_code', 'product_jan', 'product_name',
            'list_price', 'process_status')
            ->where('price_supplier_id', '=', $adminId);
        $txtProductJan = $params['product_jan'];
        $keywordsProductJan = explode(" ", $txtProductJan);
        $txtMakerFullNm = $params['maker_full_nm'];
        $keywordsMakerFullNm = explode(" ", $txtMakerFullNm);
        $txtProductName = $params['product_name'];
        $keywordsProductName = explode(" ", $txtProductName);
        $txtProductCode = $params['product_code'];
        $keywordsProductCode = explode(" ", $txtProductCode);
        $results->Where(function ($results) use ($keywordsProductJan) {
            foreach ($keywordsProductJan as $keywordProductJan) {
                if ($keywordProductJan != "") {
                    $results->orWhere('product_jan', 'LIKE', '%' . $keywordProductJan . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsMakerFullNm) {
            foreach ($keywordsMakerFullNm as $keywordMakerFullNm) {
                if ($keywordMakerFullNm != "") {
                    $results->orWhere('maker_full_nm', 'LIKE', '%' . $keywordMakerFullNm . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsProductName) {
            foreach ($keywordsProductName as $keywordProductName) {
                if ($keywordProductName != "") {
                    $results->orWhere('product_name', 'LIKE', '%' . $keywordProductName . '%');
                }
            }
        });
        $results->Where(function ($results) use ($keywordsProductCode) {
            foreach ($keywordsProductCode as $keywordProductCode) {
                if ($keywordProductCode != "") {
                    $results->orWhere('product_code', 'LIKE', '%' . $keywordProductCode . '%');
                }
            }
        });
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "true") {
            $results = $results;
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '<', 2);
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '!=', 1);
        }
        if ($params['cheetahStatus0'] == "true" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '=', 0);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '>', 0);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "true" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '=', 1);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "true") {
            $results = $results->where('cheetah_status', '=', 2);
        }
        if ($params['cheetahStatus0'] == "false" && $params['cheetahStatus1'] == "false" && $params['cheetahStatus2'] == "false") {
            $results = $results->where('cheetah_status', '>', 2);
        }
        $results = $results->get()->toArray();
        $path = storage_path('app/' . $fileName);
        $headers = array(
            'Content-Encoding: Shift-JIS',
            'Content-Type' => 'text/csv',
        );
        $csv_header = array("ステータス", "メーカー名", "品番", "JANコード", "商品名", "定価", "処理ステータス");
        $csv_header = mb_convert_encoding(implode(",", $csv_header), 'Shift-JIS', 'UTF-8');
        $file = fopen($path, 'w');
        fputs($file, $csv_header . "\r\n");
        foreach ($results as $product) {
            //Change label for cheetah_status
            if ($product['cheetah_status'] == 0) {
                $product['cheetah_status'] = '販売';
            } else {
                if ($product['cheetah_status'] == 1) {
                    $product['cheetah_status'] = '欠品';
                } else {
                    if ($product['cheetah_status'] == 2) {
                        $product['cheetah_status'] = '廃盤';
                    }
                }
            }
            //Change label for process_status
            if ($product['process_status'] == 0) {
                $product['process_status'] = 'Default';
            } else {
                if ($product['process_status'] == 1) {
                    $product['process_status'] = 'Waiting';
                } else {
                    if ($product['process_status'] == 2) {
                        $product['process_status'] = 'Approved';
                    }
                }
            }
            //Convent to encoding and put into file
            $product = mb_convert_encoding(implode(",", $product), 'Shift-JIS', 'UTF-8');
            fputs($file, $product . "\r\n");
        }
        return array('path' => $path, 'headers' => $headers, 'results' => $results);
    }

    /**
     * Put product from file .csv to table
     *
     * @param $filePath
     * @param $adminId
     * @return bool|null
     */
    public function putProductFromCSV($filePath, $adminId)
    {
        $flag = null;
        $row = 0;
        $listProductFromFile = array();
        if (($handle = fopen($filePath, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $dataConvert = array();
                foreach ($data as $value) {
                    $dataConvert[] = mb_convert_encoding($value, "UTF-8", "Shift-JIS");
                }
                if ($row == 0) // Skip header
                {
                    $row++;
                    continue;
                }
                //Change value for cheetah_status
                if ($dataConvert[0] == '販売') {
                    $dataConvert[0] = 0;
                } else {
                    if ($dataConvert[0] == '欠品') {
                        $dataConvert[0] = 1;
                    } else {
                        if ($dataConvert[0] == '廃盤') {
                            $dataConvert[0] = 2;
                        }
                    }
                }
                //Change value for process_status
                if ($dataConvert[6] == 'Default') {
                    $dataConvert[6] = 0;
                } else {
                    if ($dataConvert[6] == 'Waiting') {
                        $dataConvert[6] = 1;
                    } else {
                        if ($dataConvert[6] == 'Approved') {
                            $dataConvert[6] = 2;
                        }
                    }
                }
                //Change value list_price to null
                if ($dataConvert[5] == '') {
                    $dataConvert[5] = null;
                }
                $product = array(
                    'cheetah_status' => $dataConvert[0],
                    'maker_full_nm' => $dataConvert[1],
                    'product_code' => $dataConvert[2],
                    'product_jan' => $dataConvert[3],
                    'product_name' => $dataConvert[4],
                    'list_price' => $dataConvert[5],
                    'process_status' => $dataConvert[6],
                    'price_supplier_id' => $adminId
                );
                array_push($listProductFromFile, $product);
                $row++;
            }
        }
        fclose($handle);
        //StatementString
        $selectStatementString = 'SELECT product_code FROM mst_product WHERE product_code = ?';
        $insertStatementString = 'INSERT INTO mst_product
                                        (product_code,
                                        product_name,
                                        product_jan,
                                        list_price,
                                        maker_full_nm,
                                        cheetah_status,
                                        process_status,
                                        price_supplier_id)
                                  VALUES(?, ?, ?, ?, ?, ?, ?, ?)';
        $updateStatementString = 'UPDATE mst_product
                                    SET product_name = ?,
                                    product_jan = ?,
                                    list_price = ?,
                                    maker_full_nm = ?,
                                    cheetah_status = ?,
                                    process_status = ?
                                    WHERE product_code = ?';
        $deleteStatementString = 'DELETE FROM mst_product WHERE product_code = ?';
        //Convent StatementString to Statement
        $selectStatement = DB::getPdo()->prepare($selectStatementString);
        $insertStatement = DB::getPdo()->prepare($insertStatementString);
        $deleteStatement = DB::getPdo()->prepare($deleteStatementString);
        $updateStatement = DB::getPdo()->prepare($updateStatementString);
        //Use Transaction for big data
        DB::beginTransaction();
        try {
            foreach ($listProductFromFile as $product) {
                $selectStatement->execute([$product['product_code']]);
                $id = $selectStatement->fetchColumn();
                if ($id == false) {
                    $insertStatement->execute([
                        $product['product_code'],
                        $product['product_name'],
                        $product['product_jan'],
                        $product['list_price'],
                        $product['maker_full_nm'],
                        $product['cheetah_status'],
                        $product['process_status'],
                        $product['price_supplier_id']
                    ]);
                } else {
                    //$deleteStatement->execute([$id]);
                    $updateStatement->execute([
                        $product['product_name'],
                        $product['product_jan'],
                        $product['list_price'],
                        $product['maker_full_nm'],
                        $product['cheetah_status'],
                        $product['process_status'],
                        $product['product_code']
                    ]);
                }
            }
            DB::commit();
            $flag = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $flag = false;
        }
        return $flag;
    }
}
