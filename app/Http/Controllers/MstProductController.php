<?php
/**
 * @package App\Http\Controllers
 * @subpackage Controller
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Models\MstProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Barryvdh\Debugbar\Facade as Debugbar;

/**
 * Class MstProductController
 * @package App\Http\Controllers
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class MstProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('mst_product_index');
    }

    /**
     * Get list mst_product of the resource.
     *
     * @@param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function load(Request $request)
    {
        Debugbar::info($request);
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = array(
            'pageSize' => $request['pageSize'],
            'currentPage' => $request['pageNo'],
        );
        $objMstProduct = new MstProduct();
        $listProduct = $objMstProduct->getListMstProduct($params, $t_admin_id);
        $paginate = $listProduct->links();
        $view = view("mst_product_paginate", ['paginate' => $paginate])->render();
        return response()->json(array('data' => $listProduct, 'paginate' => $view), 200);
    }

    /**
     * Get list mst_product of the resource.
     *
     * @@param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = array(
            'pageSize' => $request['pageSize'],
            'currentPage' => $request['pageNo'],
            'cheetahStatus0' => $request['cheetahStatus0'],
            'cheetahStatus1' => $request['cheetahStatus1'],
            'cheetahStatus2' => $request['cheetahStatus2'],
            'product_jan' => $request['product_jan'],
            'maker_full_nm' => $request['maker_full_nm'],
            'product_name' => $request['product_name'],
            'product_code' => $request['product_code'],
        );
        $objMstProduct = new MstProduct();
        $listProduct = $objMstProduct->getListMstProductByRequest($params, $t_admin_id);
        $paginate = $listProduct->links();
        $view = view("mst_product_paginate", ['paginate' => $paginate])->render();
        return response()->json(array('data' => $listProduct, 'paginate' => $view), 200);
    }

    /**
     * Insert mst_product to resource.
     *
     * @@param $request
     * @return Response
     */
    public function insert(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = $request['product'];
        $objMstProduct = new MstProduct();
        $objMstProduct->insertMstProduct($params, $t_admin_id);
        return response()->json(200);
    }

    /**
     * Update mst_product to resource.
     *
     * @@param $request
     * @return Response
     */
    public function update(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = $request['product'];
        $objMstProduct = new MstProduct();
        $objMstProduct->updateMstProduct($params, $t_admin_id);
        return response()->json(200);
    }

    /**
     * Delete mst_product from resource.
     *
     * @@param $request
     * @return Response
     */
    public function delete(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = $request['product'];
        $objMstProduct = new MstProduct();
        $objMstProduct->deleteMstProduct($params, $t_admin_id);
        return response()->json(200);
    }

    /**
     * Get mst_product by ProductCode from resource.
     *
     * @@param $request
     * @return Response
     */
    public function getProductByCode(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $params = $request['product_code'];
        $objMstProduct = new MstProduct();
        $mstProduct = $objMstProduct->getMstProductByProductCode($params, $t_admin_id);
        return response()->json(array('mstProduct' => $mstProduct, 200));
    }

    /**
     * Change status product mst_product of table
     *
     * @@param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function changeStatusProduct(Request $request)
    {
        $params = array(
            'option' => $request['option'],
            'listProductSelected' => $request['listProductSelected'],
        );
        $objMstProduct = new MstProduct();
        $objMstProduct->changeStatusProduct($params);
        return response()->json(200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function exportToCSV(Request $request)
    {
        $allRequest = $request->all();
        $cheetahStatus0 = isset($allRequest['checkboxCheetahStatus0']) ? "true" : "false";
        $cheetahStatus1 = isset($allRequest['checkboxCheetahStatus1']) ? "true" : "false";
        $cheetahStatus2 = isset($allRequest['checkboxCheetahStatus2']) ? "true" : "false";
        $params = array(
            'cheetahStatus0' => $cheetahStatus0,
            'cheetahStatus1' => $cheetahStatus1,
            'cheetahStatus2' => $cheetahStatus2,
            'product_jan' => $allRequest['txtProductJan'],
            'maker_full_nm' => $allRequest['txtMakerFullNm'],
            'product_name' => $allRequest['txtProductName'],
            'product_code' => $allRequest['txtProductCode'],
        );
        $t_admin_id = Auth::id();
        $objMstProduct = new MstProduct();
        $fileName = 'report_file_csv.csv';
        $results = $objMstProduct->getProductExportToCSV($params, $t_admin_id, $fileName);
        register_shutdown_function('unlink', $results['path']);
        return response()->download($results['path'], $fileName, $results['headers']);
    }

    /**
     * @param Request $request
     * @return mixed
     * @internal param $Request
     */
    public function importFromCSV(Request $request)
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        //Get file from Request
        $fileUpload = $request->file('selectFile');
        $fileName = $fileUpload->getClientOriginalName();
        //Move file to storage/app
        $pathStorage = storage_path('app/');
        $fileUpload->move($pathStorage, $fileName);
        $path = storage_path('app/' . $fileName);
        register_shutdown_function('unlink', $path);
        //Import Data
        $objMstProduct = new MstProduct();
        $flagSuccess = $objMstProduct->putProductFromCSV($path, $t_admin_id);
        return response()->json(array('flag' => $flagSuccess), 200);
    }
}
