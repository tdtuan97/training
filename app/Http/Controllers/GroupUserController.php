<?php
/**
 * @package App\Http\Controllers
 * @subpackage Controller
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Models\GroupRule;
use App\Models\GroupUser;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class GroupUserController
 * @package App\Http\Controllers
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class GroupUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view group_user_index
     */
    public function index()
    {
        return view('group_user_index');
    }

    /**
     * Get a listing record per page.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(Request $request)
    {
        $allRequest = $request['data'];
        $params = array(
            'action' => $allRequest['action'],
            'pageSize' => $allRequest['pageSize'],
            'currentPage' => $allRequest['page'],
            'group_id' => $allRequest['group_id'],
            'group_name' => $allRequest['group_name'],
            'group_comment' => $allRequest['group_comment'],
        );
        $objGroupUser = new GroupUser();
        $listGroupUser = null;
        if ($params['action'] == 'load') {
            $listGroupUser = $objGroupUser->getListGroupUser($params);
        }
        if ($params['action'] == 'search') {
            $listGroupUser = $objGroupUser->getListGroupUserByRequest($params);
        }
        $paginate = $listGroupUser->links();
        $view = view("group_user_paginate", ['paginate' => $paginate])->render();
        return response()->json(array('data' => $listGroupUser, 'paginate' => $view), 200);
    }

    /**
     * Get a listing of the resource.
     *
     * @return Response json()
     * @internal param Request $request
     */
    public function getAllGroupUser()
    {
        $objGroupUser = new GroupUser();
        $data = $objGroupUser->getAllDataGroupUser();
        return response()->json(array('listGroupUser' => $data), 200);
    }

    /**
     * Update record insert update delete
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $allRequest = $request['group'];
        $objGroupUser = new GroupUser();
        if ($allRequest['optionUpdateGroup'] == 'delete') {
            $objGroupUser->deleteGroupUser($allRequest);
        }
        if ($allRequest['optionUpdateGroup'] == 'insert') {
            $objGroupUser->insertGroupUser($allRequest);
        }
        if ($allRequest['optionUpdateGroup'] == 'update') {
            $objGroupUser->updateGroupUser($allRequest);
        }
        return response()->json(200);
    }

    /**
     * Change rule of group
     *
     * @param Request $request
     * @return Response json()
     */
    public function changeRule(Request $request)
    {
        $listGroupSelected = $request['listGroupSelected'];
        $listRuleSelected = $request['listRuleSelected'];
        $objGroupUser = new GroupUser();
        $objGroupUser->changeRule($listGroupSelected, $listRuleSelected);
        return response()->json(200);
    }

    /**
     * Get variable by group name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroupByName(Request $request)
    {
        $params = $request['group_name'];
        $objGroupUser = new GroupUser();
        $groupUser = $objGroupUser->getGroupUserByName($params);
        return response()->json(array('group' => $groupUser, 200));
    }

    /**
     * Get variable by group id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroupById(Request $request)
    {
        $params = $request['group_id'];
        $objGroupUser = new GroupUser();
        $groupUser = $objGroupUser->getGroupUserById($params);
        return response()->json(array('group' => $groupUser, 200));
    }

    /**
     * Get list rule of group by group id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRulesByGroupId(Request $request)
    {
        $groupId = $request['group_id'];
        $objGroupRule = new GroupRule();
        $rules = $objGroupRule->getRulesByGroupId($groupId);
        return response()->json(array('rules' => $rules, 200));
    }
}
