<?php
/**
 * @package App\Http\Controllers
 * @subpackage Controller
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Models\MstUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Barryvdh\Debugbar\Facade as Debugbar;

/**
 * Class MstUserController
 * @package App\Http\Controllers
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class MstUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mst_user_index');
    }

    /**
     * Get a listing record per page.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(Request $request)
    {
        $allRequest = $request['data'];
        $params = array(
            'action' => $allRequest['action'],
            'pageSize' => $allRequest['pageSize'],
            'currentPage' => $allRequest['page'],
            't_admin_id' => $allRequest['t_admin_id'],
            'email' => $allRequest['email'],
            'activate' => $allRequest['activate'],
            'deactivate' => $allRequest['deactivate']
        );
        $objMstUser = new MstUser();
        if ($params['action'] == 'load') {
            $listMstUser = $objMstUser->getListMstUser($params);
        }
        if ($params['action'] == 'search') {
            $listMstUser = $objMstUser->getListMstUserByRequest($params);
        }
        $paginate = $listMstUser->links();
        $view = view("mst_user_paginate", ['paginate' => $paginate])->render();
        return response()->json(array('data' => $listMstUser, 'paginate' => $view), 200);
    }

    /**
     * Update record insert update delete
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $allRequest = $request['user'];
        $objMstUser = new MstUser();
        $email = "";
        $password = "";
        if ($allRequest['optionUpdateUser'] == 'delete') {
            $objMstUser->deleteMstUser($allRequest);
            return response()->json(array('email' => $email, 'password' => $password), 200);
        }
        $validator = Validator::make($allRequest, [
            'email' => 'required|email|max:20',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                if (strlen(strstr($error, 'email')) > 0) {
                    $email = $error;
                }
                if (strlen(strstr($error, 'password')) > 0) {
                    $password = $error;
                }
            }
            return response()->json(array('email' => $email, 'password' => $password), 200);
        }
        if ($allRequest['optionUpdateUser'] == 'insert') {
            $objMstUser->insertMstUser($allRequest);
        }
        if ($allRequest['optionUpdateUser'] == 'update') {
            $objMstUser->updateMstUser($allRequest);
        }
        return response()->json(array('email' => $email, 'password' => $password), 200);
    }

    /**
     * Get variable by user id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserById(Request $request)
    {
        $params = $request['t_admin_id'];
        $objMstUser = new MstUser();
        $mstUser = $objMstUser->getMstUserById($params);
        return response()->json(array('user' => $mstUser, 200));
    }

    /**
     * Get variable by email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByEmail(Request $request)
    {
        $params = $request['email'];
        $objMstUser = new MstUser();
        $mstUser = $objMstUser->getMstUserByEmail($params);
        return response()->json(array('user' => $mstUser, 200));
    }

    /**
     * Get group of user when user login
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGroupUserLogin()
    {
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $objMstUser = new MstUser();
        $groupUser = $objMstUser->getGroupUserByUserId($t_admin_id);
        $groupId = $groupUser['group_id'];
        $groupRule = $objMstUser->getRulesByGroupId($groupId);
        return response()->json(array('groupUser' => $groupUser, 'groupRule' => $groupRule), 200);
    }

    /**
     * Change status active of user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatusUser(Request $request)
    {
        $request = $request->all();
        $params = array(
            'option' => $request['option'],
            'listUserSelected' => $request['listUserSelected'],
        );
        $objMstUser = new MstUser();
        $objMstUser->changeStatusUser($params);
        return response()->json(200);

    }

    /**
     * Change group id of user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeGroupUser(Request $request)
    {
        $request = $request->all();
        $params = array(
            'groupId' => $request['groupId'],
            'listUserSelected' => $request['listUserSelected'],
        );
        $objMstUser = new MstUser();
        $objMstUser->changeGroupUser($params);
        return response()->json(200);
    }
}
