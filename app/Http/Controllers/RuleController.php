<?php
/**
 * @package App\Http\Controllers
 * @subpackage Controller
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Http\Controllers;

use App\Models\RuleUser;

/**
 * Class RuleController
 * @package App\Http\Controllers
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class RuleController extends Controller
{
    /**
     * Get listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRule()
    {
        $objRuleUser = new RuleUser();
        $data = $objRuleUser->getAllRule();
        return response()->json(array('listRuleUser' => $data), 200);
    }
}
