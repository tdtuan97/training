<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Router;
use App\Models\TMorderType;
use App\Models\VOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Barryvdh\Debugbar\Facade as Debugbar;

class OrderController extends Controller
{
    public function __construct(Router $router)
    {

    }
    /**
     * Menu action
     * @var string
     */
    private $menu = 'order';

    /**
     * message error
     * @var array
     */
    private $errors = null;

    /**
     * message success
     * $var arrray
     */
    private $successes = null;

    /**
     * staff flag
     * @var int
     */
    protected $staffFlag;

    public function index(Request $request, $viewName = "order.index", $tOrderData2 = null)
    {
        $action = $request->route()->getAction();
        $controller = class_basename($action['controller']);
        Debugbar::info($action);
        Debugbar::info($controller);
        list($controller, $action) = explode('@', $controller);
        Debugbar::info($action);
        Debugbar::info($controller);
        //Get value t_admin_id from Auth::id() when user login
        $t_admin_id = Auth::id();
        $modelTMorderType   = new TMorderType();
        $modelVorder        = new VOrder();


        $year           = $request->input('order_date_year', '');
        $month          = $request->input('order_date_month', '');
        $day            = $request->input('order_date_day', '');
        $hour           = $request->input('order_date_h', '');
        $minute         = $request->input('order_date_i', '');
        $seconds        = $request->input('order_date_s', '');

        $yearTo         = $request->input('order_date_year_to', '');
        $monthTo        = $request->input('order_date_month_to', '');
        $dayTo          = $request->input('order_date_day_to', '');
        $hourTo         = $request->input('order_date_h_to', '');
        $minuteTo       = $request->input('order_date_i_to', '');
        $secondsTo      = $request->input('order_date_s_to', '');

        $supplykb1      = $request->input('supplykb1', '');
        $supplykb2      = $request->input('supplykb2', '');
        $name           = $request->input('s_name', '');
        $supplieritemcd = $request->input('s_supplieritemcd', '');
        $janCode        = $request->input('s_jan_code', '');
        $makerCode      = $request->input('s_maker_code', '');
        $orderCode      = $request->input('s_order_code', '');
        $memo           = $request->input('s_memo', '');
        $sendFaxDate    = $request->input('s_send_fax_date', '');
        $reminderFlg1   = $request->input('reminder_flg1', '');
        $reminderFlg2   = $request->input('reminder_flg2', '');
        $mOrderTypeId   = $request->input('s_m_order_type_id', '');
        $percent        = $request->input('s_percent', '');
        $sort           = $request->input('sort', '');

        if (empty($mOrderTypeId)) {
            $mOrderTypeId = 2;
        }

        $dateFrom       = "";
        $dateTo         = "";
        if (!empty($year) && !empty($month) && !empty($day) && !empty($hour) && !empty($minute) && !empty($seconds)) {
            $dateFrom = "{$year}-{$month}-{$day} {$hour}-{$minute}-{$seconds}";
        } elseif (!empty($year) && !empty($month) && !empty($day)) {
            $dateFrom = "{$year}-{$month}-{$day} 00:00:00";
        } elseif (!empty($year) && !empty($month)) {
            $dateFrom = "{$year}-{$month}-01 00:00:00";
            $dateTo   = "{$year}-{$month}-31 00:00:00";
        }

        if (!empty($yearTo) && !empty($monthTo) && !empty($dayTo)
            && !empty($hourTo) && !empty($minuteTo) && !empty($secondsTo)) {
            $dateTo = "{$yearTo}-{$monthTo}-{$dayTo} {$hourTo}-{$minuteTo}-{$secondsTo}";
        } elseif (!empty($yearTo) && !empty($monthTo) && !empty($dayTo)) {
            $dateTo = "{$yearTo}-{$monthTo}-{$dayTo} 23:59:59";
        } elseif (!empty($yearTo) && !empty($monthTo) && empty($dateFrom)) {
            $dateTo = "{$yearTo}-{$monthTo}-01 23:59:59";
        }

        $reminderFlg = array();
        if (!empty($reminderFlg1)) {
            $reminderFlg[] = '1';
        }
        if (!empty($reminderFlg2)) {
            $reminderFlg[] = '2';
        }

        $arraySearch = [
            'order_date_from' => $dateFrom,
            'order_date_to'   => $dateTo,
            'supplykb1'       => $supplykb1,
            'supplykb2'       => $supplykb2,
            'name'            => $name,
            'supplieritemcd'  => $supplieritemcd,
            'jan_code'        => $janCode,
            'maker_code'      => $makerCode,
            'order_code'      => $orderCode,
            'memo'            => $memo,
            'send_fax_date'   => $sendFaxDate,
            'reminder_flg'    => $reminderFlg,
            'm_order_type_id' => $mOrderTypeId,
            'percent'         => $percent
        ];

        $arrayQuery  = [
            'suppliercd'      => $t_admin_id,
            'state_flg'       => 1,
            'per_page'        => $request->input('per_page', 20)
        ];

        switch ($sort) {
            case 'order_date_desc':
                $arrSort['order_date'] = 'desc';
                break;
            case 'order_date_asc':
                $arrSort['order_date'] = 'asc';
                break;
            case 'jan_code_desc':
                $arrSort['jan_code'] = 'desc';
                break;
            case 'jan_code_asc':
                $arrSort['jan_code'] = 'asc';
                break;
            default:
                $arrSort['order_date'] = 'desc';
                break;
        }
        $orderCsvCheck      = "";
        $tOrderData         = $modelVorder->getListOrder($arrayQuery, $arrSort, $arraySearch);
        if (!empty($orderCsvCheckData)) {
            $orderCsvCheck      = $orderCsvCheckData->orderCsvCheck;
        }
        $listOrderType      = $modelTMorderType->getOrderType();
        $listOrderTypeRadio = $modelTMorderType->getOrderTypeRadio();

        return view($viewName)->with(array(
            'menu'                    => $this->menu,
            'staff_flg'               => $this->staffFlag,
            'm_order_type_data'       => $listOrderType,
            'm_order_type_data_radio' => $listOrderTypeRadio,
            'percent'                 => 5,
            'form_year'               => $this->formYear(),
            'form_month'              => $this->formMonth(),
            'form_day'                => $this->formDay(),
            'form_h'                  => $this->formHour(),
            'form_i'                  => $this->formMinute(),
            'form_s'                  => $this->formSeconds(),
            'order_date_year'         => $year,
            'order_date_month'        => $month,
            'order_date_day'          => $day,
            'order_date_h'            => $hour,
            'order_date_i'            => $minute,
            'order_date_s'            => $seconds,
            'order_date_year_to'      => $yearTo,
            'order_date_month_to'     => $monthTo,
            'order_date_day_to'       => $dayTo,
            'order_date_h_to'         => $hourTo,
            'order_date_i_to'         => $minuteTo,
            'order_date_s_to'         => $secondsTo,
            'supplykb1'               => $supplykb1,
            'supplykb2'               => $supplykb2,
            's_name'                  => $name,
            's_supplieritemcd'        => $supplieritemcd,
            's_jan_code'              => $janCode,
            's_maker_code'            => $makerCode,
            's_order_code'            => $orderCode,
            's_memo'                  => $memo,
            's_send_fax_date'         => $sendFaxDate,
            'reminder_flg1'           => $reminderFlg1,
            'reminder_flg2'           => $reminderFlg2,
            's_m_order_type_id'       => $mOrderTypeId,
            's_percent'               => $percent,
            'per_page'                => $request->input('per_page', 20),
            'sort'                    => $sort,
            'orderCsvCheck'           => $orderCsvCheck,
            't_order_data'            => $tOrderData,
            'price_flg'               => auth()->user()->price_flg,
            't_order_quantity'        => null,
            't_order_detail_quantity' => null,
            'errors'                  => $this->errors,
            'successes'               => $this->successes,
            't_order_data2'           => $tOrderData2,
        ));
    }
    /**
     * Get a array number year of system
     * @param type $year
     * @return array
     */
    public function formYear($year = 2016)
    {
        for ($year; $year <= date('Y') + 1; $year++) {
            $arrayYear[] = $year;
        }
        return $arrayYear;
    }

    /**
     * Get a array number month of system
     * @param type $month
     * @return array
     */
    public function formMonth($month = 1)
    {
        for ($month = 1; $month <= 12; $month++) {
            $arrayMonth[] = $month;
        }
        return $arrayMonth;
    }

    /**
     * Get a array number month of system
     * @param type $month
     * @return array
     */
    public function formDay($day = 1)
    {
        for ($day = 1; $day <= 31; $day++) {
            $arrayDay[] = $day;
        }
        return $arrayDay;
    }

    /**
     * Get array number hour of system
     *
     * @return  array
     */
    public function formHour()
    {
        $arrHour = array();
        for ($hour = 0; $hour <= 23; $hour++) {
            $arrHour[] = str_pad($hour, 2, "0", STR_PAD_LEFT);
        }
        return $arrHour;
    }

    /**
     * Get array number minute of system
     *
     * @return  array
     */
    public function formMinute()
    {
        $arrMinute = array();
        for ($minute = 0; $minute <= 59; $minute++) {
            $arrMinute[] = str_pad($minute, 2, "0", STR_PAD_LEFT);
        }
        return $arrMinute;
    }

    /**
     * Get array number seconds of system
     *
     * @return  array
     */
    public function formSeconds()
    {
        $arrSeconds = array();
        for ($seconds = 0; $seconds <= 59; $seconds++) {
            $arrSeconds[] = str_pad($seconds, 2, "0", STR_PAD_LEFT);
        }
        return $arrSeconds;
    }
}
