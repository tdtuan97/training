<?php
/**
 * @package App\Http\Controllers
 * @subpackage Controller
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
namespace App\Http\Controllers;

/**
 * Class GroupRuleController
 * @package App\Http\Controllers
 * @copyright Copyright (c) 2019 RiverCrane! Corporation. All Rights Reserved.
 * @author Tran Tuan<tran.tuan@crivercrane.vn>
 */
class GroupRuleController extends Controller
{
    //
}
